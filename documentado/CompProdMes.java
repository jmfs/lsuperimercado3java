/**
 * Write a description of class CompProdMes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeSet;
import java.io.Serializable;
public class CompProdMes implements Serializable
{
    // instance variables - replace the example below with your own
    private double gasto;
    private int quant;
    private int comp;
    private int nClient;
   
    /**
     * Constructor for objects of class CompProdMes
     */
    public CompProdMes(){
        this.gasto=0;
        this.quant=0;
        this.comp=0;
        this.nClient=0;
    }
    
    /**
     * @param double gasto; total faturado
     * @param int quant; n de produtos comprados
     * @param int nClient; n clientes que compraram
     * Constructor for objects of class CompProdMes
     */
    public CompProdMes(double gasto, int quant, int nClient){
        this.gasto=gasto;
        this.quant=quant;
        this.comp++;
        this.nClient=nClient;
    }
    
    /**
     * @param CompProdMes c
     * Constructor for objects of class CompProdMes
     */
    public CompProdMes(CompProdMes c){
        this.gasto=c.getGasto();
        this.quant=c.getQuant();
        this.comp=c.getComp();
        this.nClient=c.getNClient();
    }
    
    /**
     * @return double; 
     * Método getGasto-total faturado
     */
    public double getGasto(){return this.gasto;}

    /**
     * @return int; 
     * Método getQuant- quantidade
     */
    public int getQuant(){return this.quant;}

    /**
     * @return int; 
     * Método getComp
     */
    public int getComp(){return this.comp;}

    /**
     * @return int; 
     * Método getNClient
     */
    public int getNClient(){return this.nClient;}
    /**
     * @return double g; 
     * Método setGasto
     */
    public void setGasto(double g){this.gasto=g;}
   /**
     * @param int q; 
     * Método getQuant- quantidade
     */
    public void setQuant(int q){ this.quant=q;}
    /**
     * @param int c; 
     * Método setComp
     */
    public void setComp(int c){ this.comp=c;}
    /**
     * @param int n; 
     * Método setNClient
     */
    public void setNClient(int n){ this.nClient=n;}
    
    /**
     * @return CompProdMes; 
     * Método clone- copia o objecto
     */
    public CompProdMes clone(){return new CompProdMes(this);}
    
    /**
     * @param double g; 
     * @param double int q;
     * Método updateMes- faz atualizaçao do mes
     */
    public void updateMes(double g, int q){
        this.gasto+=(g*q);
        this.quant+=q;
        this.comp++;
        this.nClient+=1;
    }
    
    /**
     * @param double g; 
     * @param double int q;
     * Método updateMesJaComp-
     */
    public void updateMesJaComp(double g, int q){
        this.gasto+=(g*q);
        this.quant+=q;
        this.comp++;
    }
    
    /**
     * @return String; 
     * Método info- passa a informaçao para formato "human-readble"
     */
    public String info(){
        StringBuilder s = new StringBuilder();
        s.append("\tGasto: " + String.format("%.2f",this.gasto)+";\n");
        s.append("\tQuantidade vendida: "+ this.quant +";\n");
        s.append("\tNº de Compras: "+ this.comp+";\n");
        s.append("\tNº de Clientes distintos que compraram: "+this.nClient+";\n");
        return s.toString();
    }
}
