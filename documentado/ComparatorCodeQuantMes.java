
/**
 * Write a description of class ComparatorCodeQuantMes here.
 * 
 * @author (your name) 
 * @version 27-05-2015
 */
import java.util.Comparator;
import java.io.Serializable;
public class ComparatorCodeQuantMes implements Comparator<CodeQuantMes>, Serializable {
    /**
     * @param  CodeQuantMes c1
     * @param  CodeQuantMes c2
     * @return int
     */
    public int compare(CodeQuantMes c1, CodeQuantMes c2) {
        if(c1.getQuant()<c2.getQuant()) return 1;
        if(c1.getQuant()>c2.getQuant()) return -1;
        return c1.getCode().compareTo(c2.getCode());
    }
}
