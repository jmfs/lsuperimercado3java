
/**
 * Write a description of class FichCompClient here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.io.Serializable;
import java.util.TreeSet;
public class FichCompClient implements Serializable 
{

    private String code;
    private int numProd; 
    private ArrayList<TopMes> infoMes;
    private TreeMap<String,CodeQuantMes> top;
    
    /**
     * @return ArrayList<String>
     * Método topClientProdutos
     */
    public ArrayList<String> topClientProdutos(){
        TreeSet<CodeQuantMes> topR = new TreeSet<>(new ComparatorCodeQuantMes());
        Iterator<CodeQuantMes> itK = this.top.values().iterator();
        while(itK.hasNext()){
            topR.add(itK.next().clone());
        }
        ArrayList<String> res = new ArrayList<>();
        Iterator<CodeQuantMes> it = topR.iterator();
        while(it.hasNext()){
            res.add(it.next().infoTopQuant());
        }
        return res;
    }
    
    /**
     * @return String
     * Método infoFichCompClient
     */
    public String infoFichCompClient(){
        StringBuilder s = new StringBuilder();
        s.append("Cliente: " + this.getCode() +" -> Nº de Produtos:" + this.getNumProd() +"\n");
        return s.toString();
    }
    
    /**
     * @param int mes
     * @return String
     * Método infoMes
     */
    public String infoMes(int mes){
        StringBuilder s = new StringBuilder();
        if(mes!=0){s.append("Informação do Mes " + mes+":\n");}
        s.append(this.infoMes.get(mes).info());
        return s.toString();
    }
    
    /**
     * @param int mes
     * @return boolean
     * Método infoMes
     */
    public boolean comprouMes(int mes){
        if(this.infoMes.get(mes).getComp()==0)return false;
        return true;
    }
    
    /**
     * @param String prod
     * @param int mes
     * @param double price
     * @param int quant
     * Método updateClient
     */
    public void updateClient(String prod, int mes, double price, int quant){
        //verifica se o codigo de produto existe no TreeMap
        if(this.top.containsKey(prod)){
            //se o codigo existir nao pode incrementar a variavel numProd e vai verificar no valeu do prod se no mes dado como argumento esse codigo já foi comprado
            if(this.top.get(prod).comprouMes(mes)){
                //se o prod já foi comprado mo mes entao nao se incrementa no mes a variavel de produtos distintos
                this.infoMes.get(mes).updateMesJáComprou(price, quant);
                this.infoMes.get(0).updateMesJáComprou(price, quant);
                this.top.get(prod).updateQuant(quant, price);
            }
            else{
                //se o prod numca foi comprado no mes 
                this.infoMes.get(mes).updateMesNoComprou(price, quant);
                this.infoMes.get(0).updateMesNoComprou(price, quant);
                this.top.get(prod).updateQuantMes(quant, price, mes);
            }
        }
        else{
            //se o codigo nao existir no TreeMap vai incrementar a variavel numProd
            this.numProd++;
            CodeQuantMes f = new CodeQuantMes(prod);
            f.initMeses();
            f.updateQuantMes(quant, price, mes);
            this.infoMes.get(mes).updateMesNoComprou(price, quant);
            this.infoMes.get(0).updateMesNoComprou(price, quant);
            this.top.put(prod, f);
        }
    }
    
    /**
     *
     */
    public void initClientInfoM(){for(int i=0;i<=12;i++){this.infoMes.add(i, new TopMes());}}
    
    public FichCompClient()
    {
        // initialise instance variables
        this.code="";
        this.numProd=0;
        this.infoMes= new ArrayList<>();
        this.top=new TreeMap<>();
    }
    
    /**
     * @param String client
     * Constructor for objects of class FichCompClient
     */
    public FichCompClient(String client)
    {
        // initialise instance variables
        this.code=client;
        this.numProd=0;
        this.infoMes= new ArrayList<>();
        this.top=new TreeMap<>();
    }
    
    /**
     * @param FichCompClient f
     * Constructor for objects of class FichCompClient
     */
    public FichCompClient(FichCompClient f){
        this.code=f.getCode();
        this.numProd=f.getNumProd();
        this.infoMes=f.getInfoMes();
        this.top=f.getTop();
    }
    
     /**
     * @return String
     * Método getCode
     */
    public String getCode(){return this.code;}
    
    /**
     * @return int
     * Método getNumProd
     */
    public int getNumProd(){return this.numProd;}
    
    /**
     * @return ArrayList<TopMes>
     * Método getInfoMes
     */
    public ArrayList<TopMes> getInfoMes(){
        ArrayList<TopMes> res = new ArrayList<>();
        for(int i=0;i<=12;i++){res.add(this.infoMes.get(i).clone());}
        return res;
    }
    
    /**
     * @return TreeMap<String,CodeQuantMes>
     * Método getTop
     */
    public TreeMap<String,CodeQuantMes> getTop(){
        TreeMap<String,CodeQuantMes> res = new TreeMap<>();
        Iterator<String> itK = this.top.keySet().iterator();
        Iterator<CodeQuantMes> itV = this.top.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            res.put(itK.next(), itV.next().clone());
        }
        return res;
    }
    
    /**
     * @return FichCompClient
     * Método clone
     */
    public FichCompClient clone(){return new FichCompClient(this);}
}
