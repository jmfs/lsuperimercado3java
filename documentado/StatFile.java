
/**
 * Write a description of class StatFile here.
 * 
 * @author (your name) 
 * @version 5-06-2015
 */

import java.io.Serializable;
public class StatFile implements Serializable
{
    private String path;
    private int nLinhas;
    private int nLinhasVal;
    
    private double fat;
    private int priceZero;
    
    private int nProd;  //quantidade total de produtos comprados
    private int distProd;   //numero disttinto de produtos que foram comprados
    private int nProdNoComp;  //numero de produtos nao comprados
    
    private int nClient; // numero total de clientes 
    private int nClientComp;    //numero de clientes que compraram
    private int nClientNoComp;  //numeto de cliente que nao compraram
    
    /**
     * Constructor for objects of class StatFile
     */
    public StatFile(){
        this.path="";
        this.nLinhas=0;
        this.nLinhasVal=0;
        this.fat=0;
        this.priceZero=0;
        this.nProd=0;
        this.nClient=0;
        this.nClientComp=0;
        this.distProd=0;
        this.nProdNoComp=0;
        this.nClientNoComp=0;
    }

    /**
     * @param StatFile s
     * Constructor for objects of class StatFile
     */

    public StatFile(StatFile s){
        this.path=s.getPath();
        this.nLinhas=s.getNLinhas();
        this.nLinhasVal=s.getNLinhasVal();
        this.fat=s.getFat();
        this.priceZero=s.getPriceZero();
        this.nProd=s.getNProd();
        this.nClient=s.getNClient();
        this.nClientComp=s.getNClientComp();
        this.distProd=s.getDistProd();
        this.nProdNoComp=s.getNProdNoComp();
        this.nClientNoComp=s.getNClientNoComp();
    }
    
    /**
     * @return String
     * Método getPath
     */
    public String getPath(){return this.path;}
    
    /**
     * @return int
     * Método getNLinhas
     */
    public int getNLinhas(){return this.nLinhas;}
    
    /**
     * @return int
     * Método getNLinhasVal
     */
    public int getNLinhasVal(){return this.nLinhasVal;}

    /**
     * @return double
     * Método getFat
     */
    public double getFat(){return this.fat;}

    /**
     * @return int
     * Método getPrinceZero
     */
    public int getPriceZero(){return this.priceZero;}

    /**
     * @return int
     * Método getNPrice
     */
    public int getNProd(){return this.nProd;}

    /**
     * @return int
     * Método getNClient
     */
    public int getNClient(){return this.nClient;}

    /**
     * @return int
     * Método getNClientComp
     */
    public int getNClientComp(){return this.nClientComp;}

    /**
     * @return int
     * Método getDistProd
     */
    public int getDistProd(){return this.distProd;}

    /**
     * @return int
     * Método getNProdNoComp
     */
    public int getNProdNoComp(){return this.nProdNoComp;}

    /**
     * @return int
     * Método getNClientNoComp
     */
    public int getNClientNoComp(){return this.nClientNoComp;}

    /**
     * @param String p
     * Método setPath
     */
    public void setPath(String p){this.path=p;}
    
    /**
     * @param int nLinhas
     * Método setNLinhas
     */
    public void setNLinhas(int nLinhas){this.nLinhas=nLinhas;}
    
    /**
     * @param int nLinhasVal
     * Método setNlinhasVal
     */
    public void setNLinhasVal(int nLinhasVal){this.nLinhasVal=nLinhasVal;}
    
    /**
     * @param double fat
     * Método setFat
     */
    public void setFat(double fat){this.fat=fat;}
    
    /**
     * @param int princeZero
     * Método setPriceZero
     */
    public void setPriceZero(int priceZero){this.priceZero=priceZero;}
    
    /**
     * @param int nProd
     * Método setNProd
     */
    public void setNProd(int nProd){this.nProd=nProd;}
    
    /**
     * @param int nClient
     * Método setNClient
     */
    public void setNClient(int nClient){this.nClient=nClient;}
    
    /**
     * @param int nClientComp
     * Método setNClientComp
     */
    public void setNClientComp(int nClientComp){this.nClientComp=nClientComp;}
    
    /**
     * @param int distProd
     * Método setDistProd
     */
    public void setDistProd(int distProd){this.distProd=distProd;}
    
    /**
     * @param int nProdNoComp
     * Método setNProdNoComp
     */
    public void setNProdNoComp(int nProdNoComp){this.nProdNoComp=nProdNoComp;}    
    
    /**
     * @param int nClientNoComp
     * Método setNClientNoComp
     */
    public void setNClientNoComp(int nClientNoComp){this.nClientNoComp=nClientNoComp;}   

    /**
     * @return String
     * Método toString, "Humam-readble"
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Estatísticas do Ficheiro: " + this.path +";\n");
        s.append("Foram lidas: " + this.nLinhas + ";\n");
        s.append("Foram validas: " + this.nLinhasVal + ";\n");
        s.append("Numero de Compras com preço igual a 0€: " + this.priceZero + ";\n");
        s.append("Foram faturados: " + String.format("%.2f",this.fat) + "€;\n");
        s.append("Foram comprados: " + this.nProd + " Produtos;\n");
        s.append("Foram comprados: " + this.distProd + " Produtos distitntos;\n");
        s.append("Não foram comprados: " + this.nProdNoComp + " Produtos;\n");
        s.append("Existem:  " + this.nClient + " Clientes;\n");
        s.append("Compraram: " + this.nClientComp + " Clientes;\n");
        s.append("Não compraram: " + this.nClientNoComp + " Clientes;\n");
        return s.toString();
    }
    
    public StatFile clone (){return new StatFile(this);}
}