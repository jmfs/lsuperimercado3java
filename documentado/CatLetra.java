
/**
 * Write a description of class CatLetra here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeSet;
import java.util.TreeMap;
import java.io.Serializable;

public class CatLetra implements Serializable
{
    private char letra;
    private TreeSet<String> listaC;
    
    /**
     * Constructor for objects of class CatLetra
     */
    public CatLetra(){
        this.letra='-';
        this.listaC=new TreeSet<>();
    }
    
   /**
     * @param char l
     * Método CatLetra
     */
    public CatLetra(char l){
        this.letra=l;
        this.listaC = new TreeSet<>();
    }
    
     /**
     * @param char l
     * @param TreeSet<String> lista
     * Construtor de objeto CatLetra
     */
    public CatLetra(char l, TreeSet<String> lista){
        this.letra=l;
        this.listaC = new TreeSet<>(lista);
    }
    
	/**
     * @param CatLetra l
     * Construtor de objeto CatLetra
     */
    public CatLetra(CatLetra l){
        this.letra=l.getLetra();
        this.listaC=l.getListaC();
    }
    /**
     * @return char
     * Método getLetra
     */
    public char getLetra(){return this.letra;}
    
    /**
     * @return TreeSet<String>
     * Método getListaC
     */
    public TreeSet<String> getListaC(){return new TreeSet<>(this.listaC);}

	/**
     * @param char l
     * Método set
     */
    public void set(char l){this.letra=l;}

    /**
     * @param TreeSet<String> lista
     * Método set
     */
    public void set(TreeSet<String> lista){this.listaC=new TreeSet<>(lista);}
    
    /**
     * @param CatLetra
     * Método clone, copia de objeto
     */
    public CatLetra clone(){return new CatLetra(this);}
    
    /**
     * @param String code
     * @return boolean
     * Método addCod
     */
    public boolean addCod(String code){return this.listaC.add(code);}
    
    /**
     * @param String code
     * @return boolean
     * Método existeCode
     */
    public boolean existeCode(String code){return this.listaC.contains(code);}
}


