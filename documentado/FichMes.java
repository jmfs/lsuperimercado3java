
/**
 * Write a description of class FichContb here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.Serializable;
public class FichMes implements Serializable
{
    // variaveis de instancia
    private int mes;
    private double euro_P;
    private double euro_N;
    private int tot_Prod_P;
    private int tot_Prod_N;
    private int tot_Comp_P;
    private int tot_Comp_N;
    
    /**
     * Construtor vazio para objectos da classe FichContb
     */
    public FichMes(){
        this.euro_P=0;
        this.euro_N=0;
        this.tot_Prod_P=0;
        this.tot_Prod_N=0;
        this.tot_Comp_P=0;
        this.tot_Comp_N=0;
    }
    
    /**
     * @param int mes
     * Construtor de objectos da classe FichContb
     */
    public FichMes(int mes){
        this.mes=mes;
        this.euro_P=0;
        this.euro_N=0;
        this.tot_Prod_P=0;
        this.tot_Prod_N=0;
        this.tot_Comp_P=0;
        this.tot_Comp_N=0;
    }
    
    /**
     * @param double euro_P;
     * @param double euro_N;
     * @param int tot_Prod_P;
     * @param int tot_prod_N;
     * @param int tot_Comp_P;
     * @param int tot_Comp_N
     * Construtor parameterizado para objectos da classe FichContb
     */
    public FichMes(double euro_P,double euro_N, int tot_Prod_P, int tot_prod_N, int tot_Comp_P, int tot_Comp_N){
        this.euro_P=(euro_P*tot_Prod_P);
        this.euro_N=(euro_N*tot_prod_N);
        this.tot_Prod_P=tot_Prod_P;
        this.tot_Prod_N=tot_prod_N;
        this.tot_Comp_P=tot_Comp_P;
        this.tot_Comp_N=tot_Comp_N;
    }
    
    /**
     * @param FichMes f
     * Construtor de copia para objectos da classe FichContb 
     */
    public FichMes(FichMes f){
        this.mes=f.getMes();
        this.euro_P=f.getEuro_P();
        this.euro_N=f.getEuro_N();
        this.tot_Prod_P=f.getTot_Prod_P();
        this.tot_Prod_N=f.getTot_Prod_N();
        this.tot_Comp_P=f.getTot_Comp_P();
        this.tot_Comp_N=f.getTot_Comp_N();
    }
    

	/**
     * @return int
     *  Metodo getMes
     */
    public int getMes(){return this.mes;}
    
    /**
     * @return double
     *  Metodo getEuro_P
     */
    public double getEuro_P(){return this.euro_P;}
    
    /**
     * @return double
     * Metodo getEuro_N
     */
    public double getEuro_N(){return this.euro_N;}
    
    /**
     * @return int
     * Metodo getTot_Prod_P
     */
    public int getTot_Prod_P(){return this.tot_Prod_P;}
    
    /**
     * return int
     * Metodo getTot_Prod_N
     */
    public int getTot_Prod_N(){return this.tot_Prod_N;}
    
    /**
     * return int
     * Metodo getTot_Comp_P
     */
    public int getTot_Comp_P(){return this.tot_Comp_P;}
    
    /**
     * return int
     * Metodo getTot_Comp_N
     */
    public int getTot_Comp_N(){return this.tot_Comp_N;}
    
    
    /**
     * @param int mes
     * Método setMes
     */
    public void setMes(int mes){this.mes=mes;}
    
    /**
     * @param double ep
     *  Metodo setEuro_P
     */
    public void setEuro_P(double ep){this.euro_P=ep;}
    
    /**
     * @param double en
     * Metodo setEuro_N
     */
    public void setEuro_N(double en){this.euro_N=en;}
    
    /**
     * @param int pp
     * Metodo setTot_Prod_P
     */
    public void setTot_Prod_P(int pp){this.tot_Prod_P=pp;}
    
    /**
     * @param int pn
     * Metodo setTot_Prod_N
     */
    public void setTot_Prod_N(int pn){this.tot_Prod_N=pn;}
    
    /**
     * @param int cp
     * Metodo setTot_Comp_P
     */
    public void setTot_Comp_P(int cp){this.tot_Comp_P=cp;}
    
    /**
     * @param int cn
     * Metodo setTot_Comp_N
     */
    public void setTot_Comp_N(int cn){this.tot_Comp_N=cn;}
   
    /**
     * @return Sring
     * Metodo toString, "human readble"
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\tGasto em modo P:" + this.getEuro_P()+"\n");
        sb.append("\tGasto em modo N:" + this.getEuro_N()+"\n");
        sb.append("\tQuantidade comprada em modo P:" + this.getTot_Prod_P()+"\n");
        sb.append("\tQuantidade comprada em modo N:" + this.getTot_Prod_N()+"\n");
        sb.append("\tNumero de compras em modo P:" + this.getTot_Comp_P()+"\n");
        sb.append("\tNumero de compras em modo N:" + this.getTot_Comp_N()+"\n");
        return sb.toString();
    }
    
    /**
     * @return FichMes
     * Metodo clone
     */
    public FichMes clone(){return new FichMes(this);}
    
    /**
     * @param Object obj
     * @return boolean
     * Metodo equals
     */
    public boolean equals (Object obj){
        boolean res = false;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass()) return false;
        FichMes f = (FichMes)obj;
        if(this.getMes()==f.getMes()
            &&this.getEuro_P()==f.getEuro_P() && this.getEuro_N()==f.getEuro_N()
            && this.getTot_Prod_P()==f.getTot_Prod_P() && this.getTot_Prod_N()==f.getTot_Prod_N()
            && this.getTot_Comp_P()==f.getTot_Comp_P() && this.getTot_Comp_N()==f.getTot_Comp_N()) {res = true;}
        return res;
    }
    
    /**
     * @return double
     * Metodo gastoNP
     */
    public double gastoNP(){return this.getEuro_P()+this.getEuro_N();}
    
    /**
     * @return int
     * Metodo quantNP
     */
    public int quantNP(){return this.getTot_Prod_P()+this.getTot_Prod_N();}
    
    /**
     * @return int
     * Metodo quantNP
     */
    public int compNP(){return this.getTot_Comp_P()+this.getTot_Comp_N();}
    
    /**
     * @param double ep;
     * @param double en;
     * @param int pp; 
     * @param int pn;
     * @param int cp;
     * @param int cn;
     * Metodo atualizaFichMes- faz a actualização desse mês
     */
    public void atualizaFichMes(double ep, double en, int pp, int pn, int cp, int cn){
        this.setEuro_P(this.getEuro_P()+(ep*pp));
        this.setEuro_N(this.getEuro_N()+(en*pn));
        this.setTot_Prod_P(this.getTot_Prod_P()+pp);
        this.setTot_Prod_N(this.getTot_Prod_N()+pn);
        this.setTot_Comp_P(this.getTot_Comp_P()+cp);
        this.setTot_Comp_N(this.getTot_Comp_N()+cn);
    }
}
