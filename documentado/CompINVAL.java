
/**
 * Write a description of class CompINVAL here.
 * 
 * @author (your name) 
 * @version 5-06-2015
 */

import java.io.Serializable;
public class CompINVAL implements Serializable
{
    private String codProd;
    private double price;
    private int quant;
    private char modo;
    private String codClient;
    private int mes;
   
    /**
     * Construtor vazio para objectos da classe Compra 
     */
    public CompINVAL(){
        this.codProd="";
        this.price=0;
        this.quant=0;
        this.modo='-';
        this.codClient="";
        this.mes=-1;
    }
    
    
    /**
     * Construtor parameterizado para objectos da classe Compra
     * @param String cP    
     * @param double price 
     * @param int quant 
     * @param char modo 
     * @param String cC 
     * @param int m     
     */
    public CompINVAL(String cP, double price, int quant, char modo, String cC, int m){
        this.codProd=cP;
        this.price=price;
        this.quant=quant;
        this.modo=modo;
        this.codClient=cC;
        this.mes=m;
    }
    
    /**
     * @param CompINVAL c
     * Construtor de copia para objectos da classe Compra
     */
    public CompINVAL(CompINVAL c){
        this.codProd=c.getCodProd();
        this.price=c.getPrice();
        this.quant=c.getQuant();
        this.modo=c.getModo();
        this.codClient=c.getCodClient();
        this.mes=c.getMes();
    }
    
    /**
     * @return String
     * Metodo getCodProd
     */
    public String getCodProd(){return this.codProd;}
    /**
     * @return double
     * Metodo getPrice
     */
    public double getPrice(){return this.price;}
    /**
     * @return int
     * Metodo getQuant
     */
    public int getQuant(){return this.quant;}
    /**
     * @return char
     * Metodo getModo
     */
    public char getModo(){return this.modo;}
    /**
     * @return String
     * Metodo getCodClient
     */
    public String getCodClient(){return this.codClient;}
    /**
     * @return int
     * Metodo getMes
     */
    public int getMes(){return this.mes;}
    
    /**
     * @param String c
     * Metodo setCodProd
     */
    public void setCodProd(String c){this.codClient=c;}

    /**
     * @param double price
     * Método setPrice
     */
    public void setPrice(double price){this.price=price;}

    /**
     * @param int quant
     * Método setQuant
     */
    public void setQuant(int quant){this.quant=quant;}

    /**
     * @param char m
     * Método setModo
     */
    public void setModo(char m){this.modo=modo;}

    /**
     * @param String c
     * Método setCodClient
     */
    public void setCodClient(String c){this.codClient=c;}

    /**
     * @param int m
     * Método setMes
     */
    public void setMes(int m){this.mes=m;}

    /**
     * @return CompINVAL
     * Metodo clone
     */
    public CompINVAL clone(){return new CompINVAL(this);}
    
    /**
     * @param Object obj
     * @return boolean
     * Metodo equals
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass()) return false;
        CompINVAL c =(CompINVAL)obj;
        return (this.getCodProd().equals(c.getCodProd()) && this.getPrice()==c.getPrice()
                && this.getQuant()==c.getQuant() && this.getModo()==c.getModo()
                && this.getCodClient().equals(c.getCodClient()) && this.getMes()==c.getMes());
    }
    
    /**
     * @return String
     * Metodo toString - Human readble
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("===================================================\n");
        s.append("Codigo de Produto: " + this.getCodProd() +";\n");
        s.append("Preço: " + this.getPrice() +";\n");
        s.append("Quantidade: " + this.getQuant() +";\n");
        s.append("Modo: " + this.getModo() +";\n");
        s.append("Codigo de Cliente: " + this.getCodClient() +";\n");
        s.append("Mes: " + this.getMes() +";\n");
        return s.toString();
    }
}
