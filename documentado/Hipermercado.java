
/**
 * Write a description of class Hipermercado here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Set;
import java.util.List;
import java.util.Iterator;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hipermercado implements Serializable
{
    private Catalogo catClient;
    private Catalogo catProd;
    private Contabilidade contb;
    private Compras comp;
 
    private StatFile lastFile;
    private ArrayList<CompINVAL> inv;
    
    /**
     * Constructor for objects of class Hipermercado
     */
    public Hipermercado(){
        this.catClient = new Catalogo();
        this.catProd = new Catalogo();
        this.contb=new Contabilidade();
        this.comp=new Compras();
        this.lastFile=new StatFile();
        this.inv = new ArrayList<>();
    }
    /**
     * Método getCatClient
     * @return Catalogo
     */
    public Catalogo getCatClient(){return this.catClient.clone();}
   /**
     * Método getCatProd
     * @return Catalogo
     */
    public Catalogo getCatProd(){return this.catProd.clone();}
    /**
     * Método getContb
     * @return Contabilidade
     */
    public Contabilidade getContb(){return this.contb.clone();}
    /**
     * Método getComp
     * @return Compras
     */
    public Compras getComp(){return this.comp.clone();}
    /**
     * Método getLastFile
     * @return StatFile
     */
    public StatFile getLastFile(){return this.lastFile.clone();}
    /**
     * Método getInv
     * @return ArrayList<CompINVAL> 
     */
    public ArrayList<CompINVAL> getInv(){
        ArrayList<CompINVAL> res = new ArrayList<>();
        Iterator<CompINVAL> it = this.inv.iterator();
        while(it.hasNext()){
            res.add(it.next().clone());
        }
        return res;
     }
    
    /**
     * Método setLastFile
     * @param StatFile s
     */
    public void setLastFile(StatFile s){this.lastFile = s;}
    
    //--------------Catalogos Clientes -------------------
    /**
     * Método addCodClient
     * @param Sring code
     */
    public void addCodClient(String code){this.catClient.addCod(code);}
    /**
     * Método addListaClient
     * @param TreeSet<String> lista
     */
    public void addListaClient(TreeSet<String> lista){
        for(String c : lista){this.addCodClient(c);}
    }
    
    /**
     * Método addListaClient
     * @param String code
     * @return boolean
     */
    public boolean existeClient(String code){return this.catClient.existeCode(code);}
    
    //--------------Catalogos Produtos -------------------
    
    /**
     * Método addCodProd
     * @param String code
     */
    public void addCodProd(String code){this.catProd.addCod(code);}
    
    /**
     * Método addListaProd
     * @param TreeSet<String> lista
     */
    public void addListaProd(TreeSet<String> lista){
        for(String c : lista){this.addCodProd(c);}
    }

    /**
     * Método existeProd
     * @param String code
     * @return boolean
     */
    public boolean existeProd(String code){return this.catProd.existeCode(code);}
    
    //--------------Contabilidade---------------
    
    /**
     * Método initContb
     * @param String code
     */
    public void initContb(String code){
        this.contb.initContbCode(code);
    }
    
    /**
     * Método carregaContb
     * @param String codProd 
     * @param double price
     * @param int quant
     * @param char modo
     * @param int mes  
     */
    public void carregaContb(String codProd, double price, int quant, char modo, int mes){
        switch(modo){
            case 'P' : {
                this.contb.updateCodeContb(codProd,mes, price, 0, quant, 0, 1, 0);
                break;
            }
            
            case 'N' : {
                this.contb.updateCodeContb(codProd,mes,0 , price,0 , quant, 0, 1);
                break;
            }
        }
    }
    
    /**
     * Método nuncaCompAnual
     * @return Set<String>
     */
    public Set<String> nuncaCompAnual(){
        return this.contb.nuncaComprados();
    }
    
    //Falta ver exceptionaqui
    
    /**
     * Método infoMesesContbCode
     * @param  String prod
     * @return  ArrayList<String>
     */
    public ArrayList<String> infoMesesContbCode(String prod){
        return this.contb.infoMesesContb(prod);
    }
  
  /**
     * Método getDistProd
     * @return  int
     */
    public int getDistProd(){return this.contb.nProdComp();}
    
    /**
     * Método getNProdNoComp
     * @return  int
     */
    public int getNProdNoComp(){return this.contb.nProdNoComp();}
    
    //-------------Compras--------------------
    
    /**
     * Método initCompras
     * @param  String listaClientes
     */
    public void initCompras(String listaClientes){this.comp.initCompClient(listaClientes);}
    
    /**
     * Método updateCompras
     * @param String codProd 
     * @param double price   
     * @param int quant     
     * @param Sting codClient 
     * @param int mes   
     */
    public void updateCompras(String codProd, double price, int quant, String codClient, int mes){
        this.comp.updateCompras(codProd, price, quant, codClient,mes);
    }
    
    /**
     * Método clientNuncaComp
     * @return  Set<String>
     */
    public Set<String> clientNuncaComp(){
        return this.comp.nuncaCompraram();
    }

    /**
     * Método infoMesesComp
     * @param  int mes
     * @return  String
     */
    public String infoMesComp(int mes) throws NaoExisteMesException{
        return this.comp.infoMes(mes);
    }
    
    /**
     * Método infoMesesCompClient
     * @param  String client
     * @return  ArrayList<String>
     */
    public ArrayList<String> infoMesCompClient(String client) throws NaoExisteClienteException{
        return this.comp.infoMesesCliente(client);
    }
    
/**
     * Método infoMesesCompProd
     * @param  String prod
     * @return  ArrayList<String>
     */
    public ArrayList<String> infoMesCompProd(String prod) throws NaoExisteProdutoException,ProdutoSemFichaException{
        if(!this.catProd.existeCode(prod)) {throw new NaoExisteProdutoException("O produto " + prod + "nao existe;");}
        return this.comp.infoMesesProd(prod);
    }
    
    /**
     * Método topProdutosDoClient
     * @param  String client
     * @return  ArrayList<String>
     */
    public ArrayList<String> topProdutosDoClient(String client) throws NaoExisteClienteException,ClienteSemFichaException  {
        return this.comp.getProdutoTopClientes(client);
    }
    
    /**
     * Método topProdutosDoProduto
     * @param  String prod
     * @return  ArrayList<String>
     */
    public ArrayList<String> topClientesDoProduto(String prod) throws NaoExisteProdutoException{
        return this.comp.getClientTopProd(prod);
    }
    /**
     * Método topProdutos
     * @return  ArrayList<String>
     */
    public ArrayList<String> topProdutos(){
        return this.comp.getProdTop();
    }
    
    /**
     * Método topClientes
     * @return  ArrayList<String>
     */
    public ArrayList<String> topClientes(){
        return this.comp.getClientTop();
    }
    
    /**
     * Método nClient
     * @return  int
     */
    public int nClient(){return this.comp.sizeCompClient();}
    
    /**
     * Método nClientComp
     * @return  int
     */
    public int nClientComp(){return this.comp.nClientComp();}
  
    /**
     * Método nClientNoComp
     * @return  int
     */
    public int nClientNoComp(){return this.comp.nClientNoComp();}
    
    //----------Geral------------
    
    /**
     * Método save
     * @param String path
     * @return String
     */
    public String save(String path){
        StringBuilder s = new StringBuilder();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
            
            oos.writeObject(this);
            oos.close();
           
        } catch (FileNotFoundException ex) {
            s.append("Nao foi encontrado o ficheiro com o path " + path + "\n");
            return s.toString();
        } catch (IOException ex) {
            return ex.getMessage();
        }
        return s.append("Estado guardado em : " + path+";\n").toString();
    }
    
    /**
     * Método load
     * @param String path
     * @return Hipermercado
     */
    public Hipermercado load(String path){
        Hipermercado geo = new Hipermercado();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
            geo=((Hipermercado)ois.readObject());
            ois.close();
            return geo;
            }
        catch(StreamCorruptedException ex){
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Método addCompInv
     * @param CompINVAL c
     * @return boolean
     */
    public boolean addCompInv(CompINVAL c){return this.inv.add(c);}
    
    /**
     * Método statHiper
     * @return String
     */
    public String statHiper(){
        StringBuilder s = new StringBuilder();
        s.append("=========================================================\n");
        s.append("  Mes   |   Nº Compras  |  Nº Clientes  |    Faturação   \n");
        s.append("========|===============|===============|================\n");
        s.append(" Mes 1  |     " +this.comp.getMesComp(1)  +  "     |      " + this.comp.getMesCl(1)  + "     |   " +  String.format("%.2f",this.comp.getMesFat(1))  + "€   \n");
        s.append(" Mes 2  |     " +this.comp.getMesComp(2)  +  "     |      " + this.comp.getMesCl(2) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(2))  + "€   \n");
        s.append(" Mes 3  |     " +this.comp.getMesComp(3)  +  "     |      " + this.comp.getMesCl(3) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(3))  + "€   \n");
        s.append(" Mes 4  |     " +this.comp.getMesComp(4)  +  "     |      " + this.comp.getMesCl(4) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(4))  + "€   \n");
        s.append(" Mes 5  |     " +this.comp.getMesComp(5)  +  "     |      " + this.comp.getMesCl(5) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(5))  + "€   \n");
        s.append(" Mes 6  |     " +this.comp.getMesComp(6)  +  "     |      " + this.comp.getMesCl(6) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(6))  + "€   \n");
        s.append(" Mes 7  |     " +this.comp.getMesComp(7)  +  "     |      " + this.comp.getMesCl(7) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(7))  + "€   \n");
        s.append(" Mes 8  |     " +this.comp.getMesComp(8)  +  "     |      " + this.comp.getMesCl(8) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(8))  + "€   \n");
        s.append(" Mes 9  |     " +this.comp.getMesComp(9)  +  "     |      " + this.comp.getMesCl(9) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(9))  + "€   \n");
        s.append(" Mes 10 |     " +this.comp.getMesComp(10) +  "     |      " + this.comp.getMesCl(10) + "     |   " +  String.format("%.2f",this.comp.getMesFat(10)) + "€   \n");
        s.append(" Mes 11 |     " +this.comp.getMesComp(11) +  "     |      " + this.comp.getMesCl(11) + "     |   " +  String.format("%.2f",this.comp.getMesFat(11)) + "€   \n");
        s.append(" Mes 12 |     " +this.comp.getMesComp(12) +  "     |      " + this.comp.getMesCl(12) + "     |   " +  String.format("%.2f",this.comp.getMesFat(12)) + "€   \n");
        s.append("=========================================================\n");
        s.append("Nº total de Compras: " + this.comp.getMesComp(0) +";\n");
        s.append("Nº total de Clientes: " + this.comp.getMesCl(0) +";\n");
        s.append("Total Faturado: " + String.format("%.2f",this.comp.getMesFat(0)) +"€;\n");
        return s.toString();
    }
    
    /**
     * Método compinv
     * @return ArrayList<String> 
     */
    public ArrayList<String> compinv(){
        ArrayList<String> res = new ArrayList<>();
        for(CompINVAL c : this.inv){
            res.add(c.toString());
        }
        return res;
    }
}
