
/**
 * Write a description of class FichCompProd here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.Serializable;
import java.util.TreeSet;
public class FichCompProd implements Serializable
{
    
    private String code;
    private ArrayList<CompProdMes> meses; 
    private TreeMap<String,CodeQuantMes> client;
    
    /**
     * Constructor for objects of class FichCompProd
     */
    public FichCompProd(){
        this.code="";
        this.meses = new ArrayList<>();
        this.client = new TreeMap<>();
    }

    /**
     * @param String code
     * Construtor de classe FichCompProd
     */
    public FichCompProd(String code){
        this.code=code;
        this.meses = new ArrayList<>();
        this.client = new TreeMap<>();
    }
    
    /**
     * @param FichCompProd f
     * Contrutor de copia da classe FichCompProd
     */
    public FichCompProd(FichCompProd f){
        this.code=f.getCode();
        this.meses = f.getMeses();
        this.client = f.getClient();
    }

    /**
     * Método getCode
     * @return String
     */
    public String getCode(){return this.code;}
   
   /**
     * Método getMeses
     * @return ArrayList<CompProdMes>
     */
    public ArrayList<CompProdMes> getMeses(){
        ArrayList<CompProdMes> novo = new ArrayList<>();
        for(int i=0;i<=12;i++){
            novo.add(this.meses.get(i).clone());  
        }
        return novo;
    }
    
    /**
     * Método getClient
     * @return TreeMap<String,CodeQuantMes>
     */
    public TreeMap<String,CodeQuantMes> getClient(){
        TreeMap<String,CodeQuantMes> novo = new TreeMap<>();
        Iterator<String> itK = this.client.keySet().iterator();
        Iterator<CodeQuantMes> itV = this.client.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            novo.put(itK.next(), itV.next().clone());
        }
        return novo;
    }
   
   /**
     * Método setCode
     * @param String code 
     */
    public void setCode(String code){this.code=code;}
    
     /**
     * Método setClientP
     * @param ArrayList<CompProdMes> m 
     */
    public void setClientP(ArrayList<CompProdMes> m){
        ArrayList<CompProdMes> novo = new ArrayList<>();
        for(int i=0;i<=12;i++){
            novo.add(i,m.get(i).clone());  
        }
        this.meses=novo;
    }
    
    /**
     * Método setClient
     * @param TreeMap<String,CodeQuantMes> cl 
     */
    public  void setClient(TreeMap<String,CodeQuantMes> cl){
        TreeMap<String,CodeQuantMes> novo = new TreeMap<>();
        Iterator<String> itK = cl.keySet().iterator();
        Iterator<CodeQuantMes> itV = cl.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            novo.put(itK.next(), itV.next().clone());
        }
        this.client=novo;
    }
    
    /**
     * Clone
     * 
     * @return FichCompProd (copia do que recebe a mensagem)
     */
    public FichCompProd clone(){return new FichCompProd (this);}
    
    /**
     * ToString
     * 
     *@return String (info do Ficha de Compras e Produtos)
     */
    public String toString(){
        StringBuilder s = new StringBuilder();        
        s.append("Code: " + this.getCode() + "\n");
        for(CompProdMes c : this.meses){s.append( c.toString() + ";\n");}
        s.append ("Todos os clientes que fizeram compras\n");
        Iterator<String> itK = this.client.keySet().iterator();
        Iterator<CodeQuantMes> itV = this.client.values().iterator();
        while(itK.hasNext() && itV.hasNext()){   
            s.append("Code: " +itK.next() + "\n" + itV.next().toString()+"\n");
        }
        return s.toString();   
    }
    
    /**
     * Metodo que retorna o numero de clientes que compraram em normal que recebe a mensagem
     * 
     * @return int (numero de clientes com compras normais)
     */
    public int nClient(){return this.client.size();}
  
    public void initMesCtr(){for(int i=0;i<=12;i++){this.meses.add(i, new CompProdMes());}}
    
    /**
     * Método updateProd
     * @param String client 
     * @param double price
     * @param int quant 
     * @param int m    
     */
    public void updateProd(String client , double price, int quant, int m){
        if(this.client.containsKey(client)){
            if(this.client.get(client).comprouMes(m)){
                //o cliente existe no TreeMap e já comprou no mes nao vai ser incrementado no mes
                //isto pode ser otimizado
                this.client.get(client).updateQuant(quant,price);
                this.meses.get(m).updateMesJaComp(price,quant);
                this.meses.get(0).updateMesJaComp(price,quant);
            }
            else{
                //o client existe no TreeMap mas nunca comprou no mes vai incrementar a variavel nClient no mes
                this.client.get(client).updateQuantMes(quant,price, m);
                this.meses.get(m).updateMes(price, quant);
                this.meses.get(0).updateMes(price, quant);
            }
        }
        else{
            //o client nao existe na lista ou seja nunca comprou tem de fazer update a tudo
            CodeQuantMes c = new CodeQuantMes(client);
            c.initMeses();
            c.updateQuantMes(quant,price, m);
            this.client.put(client,c);
            this.meses.get(m).updateMes(price, quant);
            this.meses.get(0).updateMes(price, quant);
        }
    }
    
    /**
     * Metodo topProdutoCliente
     * @return ArrayList<String>
     */
    public ArrayList<String> topProdutoCliente(){
        TreeSet<CodeQuantMes> topR = new TreeSet<>(new ComparatorCodeQuantMes());
        Iterator<CodeQuantMes> itK = this.client.values().iterator();
        while(itK.hasNext()){
            topR.add(itK.next().clone());
        }
        ArrayList<String> res = new ArrayList<>();
        Iterator<CodeQuantMes> it = topR.iterator();
        while(it.hasNext()){
            res.add(it.next().infoTopQuant());
        }
        return res;
    }
    
    /**
     * Metodo getQuantAnual
     * @return int
     */
    public int getQuantAnual(){return this.meses.get(0).getQuant();}
    
    /**
     * Método infoFichCompProd
     * @return String
     */
    public String infoFichCompProd(){
        StringBuilder s = new StringBuilder();
        CompProdMes m =  this.meses.get(0);
        s.append("Produto: " + this.code + "; Quantidade: " + m.getQuant() + "; Gasto: " + String.format("%.2f",m.getGasto()) + "€; Nº de clientes: " + m.getNClient() + ";\n");
        return s.toString();
    }
    
    /**
     * Metodo infoMes
     * @param int mes
     * @return String
     */
    public String infoMes(int mes){
        StringBuilder s = new StringBuilder();
        s.append("Informação do Mes " + mes+":\n");
        s.append(this.meses.get(mes).info());
        return s.toString();
    }
}
    

