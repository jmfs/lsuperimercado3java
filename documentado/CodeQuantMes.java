
/**
 * Write a description of class CodeQuantMesMes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.Serializable;
public class CodeQuantMes implements Serializable
{
    private String code;
    private int quant;
    private double gasto;
    private ArrayList<Integer> meses;
    
    /**
     * Constructor for objects of class CodeQuantMes
     */
    public CodeQuantMes(){
        this.code="";
        this.quant=0;
        this.gasto=0;
        this.meses=new ArrayList<>();
    }
    /**
     * @param String code
     * Construtor de classe objeto CodeQuantMes
     */
    public CodeQuantMes(String code){
        this.code=code;
        this.quant=0;
        this.gasto=0;
        this.meses=new ArrayList<>();
    }
    
    /**
     * @param String code
     * @param int quant
     * @param double price
     * Construtor de classe objeto CodeQuantMes
     */
    public CodeQuantMes(String code, int quant, double price){
        this.code=code;
        this.quant=quant;
        this.gasto=(quant*price);
        this.meses=new ArrayList<>();
    }
    
    /**
     * @param CodeQuantMes cqm
     * Construtor de classe objeto CodeQuantMes
     */
    public CodeQuantMes(CodeQuantMes cqm){
        this.code=cqm.getCode();
        this.quant=cqm.getQuant();
        this.gasto=cqm.getGasto();
        this.meses=cqm.getMeses();
    }
    
    /**
     * @return String
     * Método getCode
     */
    public String getCode(){return this.code;}

    /**
     * @return int
     * Método getQuant
     */
    public int getQuant(){return this.quant;}

    /**
     * @return double
     * Método getGasto
     */
    public double getGasto(){return this.gasto;}

    /**
     * @return ArrayList<Integer>
     * Método getMeses
     */
    public ArrayList<Integer> getMeses(){return new ArrayList<>(this.meses);}

    /**
     * @param String code
     * Método setCode
     */
    public void setCode(String code){this.code=code;}

    /**
     * @param int quant
     * Método setQuant
     */
    public void setQuant(int quant){this.quant=quant;}

    /**
     * @param double gasto
     * Método setǴasto
     */
    public void setGasto(double gasto){this.gasto=gasto;}

    /**
     * @param ArrayList<Integer> meses
     * Método setMeses
     */
    public void setMeses(ArrayList<Integer> meses){this.meses=new ArrayList<>(meses);}

    /**
     * @return int
     * Método sizeMeses
     */
    public int sizeMeses(){return this.meses.size();}
    
    /**
     * @return CodeQuantMes
     * Método clone
     */
    public CodeQuantMes clone(){ return new CodeQuantMes(this);}
    
    /**
     * @param Object obj
     * @return boolean
     * Método equals
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass()) return false;
        CodeQuantMes cq =(CodeQuantMes)obj;
        if(!this.getCode().equals(cq.getCode()) && this.getQuant()!=cq.getQuant()
            && this.sizeMeses()!=cq.sizeMeses() && this.gasto!=cq.getGasto()) return false;
        Iterator<Integer> it1 = this.meses.iterator();
        Iterator<Integer> it2 = cq.getMeses().iterator();
        while(it1.hasNext() && it2.hasNext()){it1.next().equals(it2.next());}
        return res; 
    }
    
    /**
     * @return String
     * Metodo toString, Human readble
     */
    public String toString(){
        int i=1;
        StringBuilder s = new StringBuilder();
        s.append("Code: " + this.getCode() +"\n");
        s.append("Quantidade: " + this.getQuant() +"\n");
        s.append("Gasto: " + this.getGasto() +"\n");
        Iterator<Integer> it = this.meses.iterator();
        while(it.hasNext()){s.append("Mes " + i +": " + it.next()+";\n");i++;}
        return s.toString();
    }
    
    /**
     * @param int m
     * @return boolean
     * Metodo equals
     */
    public boolean comprouMes(int m){
        if(this.meses.get(m)==0) return false;
        return true;
    }
    
    /**
     * @param int q
     * @param double prince
     * Metodo updateQuant
     */
    public void updateQuant(int q , double price){
        this.gasto+=(q*price);
        this.setQuant(this.getQuant()+q);
    }
    
    /**
     * @param int q
     * @param double prince
     * @param int m
     * Metodo updateQuantMes
     */
    public void updateQuantMes(int q,double price, int m){
        this.gasto+=(q*price);
        this.meses.add(m,1);
        this.setQuant(this.getQuant()+q);
    }
    
    /**
     * @param CodeQuantMes cq
     * @return int
     * Metodo compareTo
     */
    public int compareTo(CodeQuantMes cq){
        return this.getCode().compareTo(cq.getCode());
    }
    
    /**
     * Metodo initMeses
     */
    public void initMeses(){for(int i=0;i<=12;i++){this.meses.add(i,0);}}
    
    /**
     * @return String
     * Metodo infoTopQuant
     */
    public String infoTopQuant(){
        StringBuilder s = new StringBuilder();
        s.append("Produto: " + this.code + " -> Quantidade:" + this.quant +" Gasto: " + String.format("%.2f",this.gasto) +"€\n");
        return s.toString();
    }
}
