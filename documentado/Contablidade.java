/**
 * Write a description of class Contabilidade here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.Serializable;
import java.util.TreeSet;
import java.util.Set;
public class Contabilidade implements Serializable
{
    // variaveis de instancia
    private TreeMap<String,FProdContb> contb;

    /**
     * Construtor vazio para objectos da classe Contabilidade
     */
    public Contabilidade(){
        this.contb=new TreeMap<>();
    }
    
    /**
     * @param TreeMap<String,FProdContb> c
     * Construtor parameterizado para objectos da classe Contabilidade
     */
    public Contabilidade(TreeMap<String,FProdContb> c){
        TreeMap<String,FProdContb> res = new TreeMap<>();
        Iterator<String> it = c.keySet().iterator();
        while(it.hasNext()){
            String m = it.next();
            try{
                res.put(m,c.get(m).clone());
            }
            catch(NullPointerException e){
                res.put(m,null);
            }
        }
        this.contb=res;
    }
    
    /**
     * @param Contabilidade c
     * Construtor de copia para objectos da classe Contabilidade
     */
    public Contabilidade(Contabilidade c){
        this.contb = c.getContb();
    }
    
    /**
     * @return TreeMap<String,FProdContb>
     * Metodo getContb
     */
    public TreeMap<String,FProdContb> getContb(){
        TreeMap<String,FProdContb> res = new TreeMap<>();
        Iterator<String> it = this.contb.keySet().iterator();
        while(it.hasNext()){
            String m = it.next();
            try{
                res.put(m,this.contb.get(m).clone());
            }
            catch(NullPointerException e){
                res.put(m,null);
            }
        }
        return res;
    }
    
    /**
     * @param TreeMap<String,FProdContb> c
     * Metodo setContb
     */
    public void setContb(TreeMap<String,FProdContb> c){
        TreeMap<String,FProdContb> res = new TreeMap<>();
        Iterator<String> it = c.keySet().iterator();
        while(it.hasNext()){
            String m = it.next();
            try{
                res.put(m,c.get(m).clone());
            }
            catch(NullPointerException e){
                res.put(m,null);
            }
        }
        this.contb=res;
    }
    
    /**
     * @return String
     * Metodo toString - "Human-readble"
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        Iterator<String> it = this.contb.keySet().iterator();
        while(it.hasNext()){
            String m = it.next();
            s.append("Codigo "+ m + "\n"+ this.contb.get(m).toString() + "\n"); 
        }
        return s.toString();
    }

    /**
     * @return Contablidade
     * Metodo clone
     */
    public Contabilidade clone(){return new Contabilidade(this);}
    
    /**
     * @param Object obj
     * @return boolean
     * Metodo equals- confirma se ambos os objetos sao ou nao iguais
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        Contabilidade c = (Contabilidade)obj;
        Iterator<String> it1 = this.contb.keySet().iterator();
        Iterator<String> it2 = c.getContb().keySet().iterator();
        if(this.sizeContb()!=c.sizeContb())return false;
        while(res && it1.hasNext() && it2.hasNext()){
            String m1 = it1.next();
            String m2 = it2.next();
            if(res=(m1.equals(m2))){
                res=this.contb.get(m1).equals(c.getContb().get(m2));
            }
        }
        return res;
    }
    
    /**
     * @return int
     * Metodo sizeContb
     */
    public int sizeContb(){return this.contb.size();}
    

     /**
     * @param String code
     * Metodo initContbCode
     */
    public void initContbCode(String code){
        this.contb.put(code,null);
    }
    
    /**
     * @param TreeSet<String> lista
     * Metodo initContb
     */
    public void initContb(TreeSet<String> lista){
        for(String code : lista){
            this.initContbCode(code);
        }
    }
    
    /**
     * @param String code
     * @return FProdContb
     * Metodo getFichCode, atraves de um codigo devolvenos a Ficha
     */
    public FProdContb getFichCode(String code){
        try {
            return this.contb.get(code).clone();
        }
        catch (NullPointerException ex){
            return null;
        }
    }
    
    /**
     * @return TreeSet<String>
     * Metodo getNoFich- produtos nao comprados, nao teem ficha
     */
    public TreeSet<String> getNoFich(){
        TreeSet<String> res = new TreeSet<>();
        Iterator<String> it = this.contb.keySet().iterator();
        while(it.hasNext()){
            String code = it.next();
            if(this.contb.get(code) == null){
                res.add(code);
            }
        }
        return res;
    }
    
    /**
     * @return Set<String>
     * Metodo nuncaComprados- produtos nao comprados
     */
    public Set<String> nuncaComprados(){
        return this.getNoFich();
    }
   
    /**
     * @param String code;
     * @param int m;
     * @param double ep;
     * @param double en;
     * @param int pp; 
     * @param int pn;
     * @param int cp;
     * @param int cn;
     * Metodo updateCodeContb, faz a atualizaçao
     */
    public void updateCodeContb(String code,int m,double ep, double en, int pp, int pn, int cp, int cn){
        if(this.contb.get(code)==null){
            FProdContb cm = new FProdContb(code);
            cm.initMeses();
            cm.atualizaMes(m,ep,en,pp,pn,cp,cn);
            this.contb.put(code,cm);
        }
        else {
            this.contb.get(code).atualizaMes(m,ep,en,pp,pn,cp,cn);
        }
    }

    /**
     * @param String code;
     * @return ArrayList<String>
     * Metodo infoMesesContb, da-nos a informaçao sobre contablidade
     */
    public ArrayList<String> infoMesesContb(String code){
        return this.contb.get(code).infoMes();
    }
    
    /**
     * @return int
     * Metodo nProdComp, numero de produtos comprados
     */
    public int nProdComp(){
        int res=0;
        Iterator<String> it = this.contb.keySet().iterator();
        while(it.hasNext()){
            String code = it.next();
            if(this.contb.get(code) != null) res++;
        }
        return res;
    }