
/**
 * Write a description of class ContbMes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeSet;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.NullPointerException;
import java.io.Serializable;
public class FProdContb implements Serializable
{
    // Variaveis de instancia
    private String code;
    private HashMap<Integer,FichMes> prodMes;

    /**
     * Constructor for objects of class ContbMes
     */
    public FProdContb(){
        this.code="";
        this.prodMes = new HashMap<>();
    }

    /**
     * @param String code
     * Constructor for objects of class ContbMes
     */
    public FProdContb(String code){
        this.code=code;
        this.prodMes = new HashMap<>();
    }
    
    /**
     * @param String code
     * @param HashMap<Integer,FichMes> meses
     * Construtor parameterizado para objectos da classe ContbMes
     */
    public FProdContb(String code,HashMap<Integer,FichMes> meses){
        this.code=code;
        HashMap<Integer,FichMes> novo = new HashMap<>();
        Iterator<Integer> itK =  meses.keySet().iterator();
        Iterator<FichMes> itV =  meses.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            novo.put(itK.next(),itV.next().clone());
        }
        this.prodMes = novo;
    }

    /**
     * @param FProdContb cm
     * Construtor de copia para objectos da classe ContbMes
     */
    public FProdContb(FProdContb cm){
        this.code=cm.getCode();
        this.prodMes=cm.getProdMes();
    }

    /**
     * @return String
     * Metodo getCode
     */
    public String getCode(){return this.code;}
    
    /**
     * @return HashMap<Integer,FichMes>
     * Metodo getProdMes
     */
    public HashMap<Integer,FichMes> getProdMes(){
        HashMap<Integer,FichMes> novo = new HashMap<>();
        Iterator<Integer> itK = this.prodMes.keySet().iterator();
        Iterator<FichMes> itV = this.prodMes.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            novo.put(itK.next(), itV.next().clone());
        }
        return novo;
    }
    
     /**
      * @param String c
     * Metodo setProdMes
     */
    public void setCode(String c){this.code=c;}
    
    /**
     * @param (HashMap<Integer,FichMes> pm)
     * Metodo setProdMes
     */
    public void setProdMes(HashMap<Integer,FichMes> pm){
        HashMap<Integer,FichMes> novo = new HashMap<>();
        Iterator<Integer> itK = pm.keySet().iterator();
        Iterator<FichMes> itV = pm.values().iterator();
        while(itK.hasNext() && itV.hasNext()){
            novo.put(itK.next(), itV.next().clone());
        }
        this.prodMes=novo;
    }
   
    /**
     * @return String
     * Metodo toString "Human-readble"
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Ficha do produto "+ this.getCode() + ":\n");
        Iterator<Integer> it = this.prodMes.keySet().iterator();
        while(it.hasNext()){
            Integer m = it.next();
            sb.append("Mes: " + m + "\n");
            try{
                sb.append(this.prodMes.get(m).toString());
            }
            catch(NullPointerException ex){
                sb.append("Nao comprou");
            };
        }
        return sb.toString();
    }
    
    /**
     * @return ProdContb
     * Metodo clone
     */
    public FProdContb clone(){return new FProdContb(this);}
    
    /**
     * @param boolean
     * Metodo equals
     */
    public boolean equals (Object obj){
        boolean res = true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass()) return false;
        FProdContb f = (FProdContb)obj;
        if(!this.getCode().equals(f.getCode()) && this.sizeProdMes()!=f.sizeProdMes()) return false;
        
        Iterator<Integer> it1 = this.getProdMes().keySet().iterator();
        Iterator<Integer> it2 = f.getProdMes().keySet().iterator();
        while(res && it1.hasNext() && it2.hasNext()){
            Integer n1 = it1.next();
            Integer n2 = it2.next();
            res = (n1.equals(n2) && this.getProdMes().get(n1).equals(f.getProdMes().get(n2))); 
        }
        return res;
    }
    
    /**
     * @return int
     * Metodo sizeContbMes
     */
    public int sizeProdMes(){return this.prodMes.size();}
    
    /**
     * M+etodo initMeses, faz a inicializaçao dos meses
     */
    public void initMeses(){
        for(int i=1;i<=12;i++){
            this.prodMes.put(i,new FichMes(i));
        }
    }
    
    /**
     * @param int m;
     * @param double ep;
     * @param double en;
     * @param int pp; 
     * @param int pn;
     * @param int cp;
     * @param int cn;
     * Metodo atualizaContMes, faz a atualizaçao da ficha mensal
     */
    public void atualizaMes(int m, double ep, double en, int pp, int pn, int cp, int cn){
        this.prodMes.get(m).atualizaFichMes(ep,en,pp,pn,cp,cn);
    }
    
    /**
     * @param int mes
     * @return double
     * Metodo getMesEN
     */
    public double getMesEN(int mes){
        try{
            return this.prodMes.get(mes).getEuro_N();
        }catch(NullPointerException e){return 0;}
    }
    
    /**
     * @param int mes
     * @return double
     * Metodo getMesEP
     */
    public double getMesEP(int mes){
        try{
            return this.prodMes.get(mes).getEuro_P();
        }catch(NullPointerException e){return 0;}
    }
    
    /**
     * @param int mes
     * @return int
     * Metodo getMesPN
     */
    public int getMesPN(int mes){
        try{
            return this.prodMes.get(mes).getTot_Prod_N();
        }catch(NullPointerException e){return 0;}
    }
    
    /**
     * @param int mes
     * @return int
     * Metodo getMesPP
     */
    public int getMesPP(int mes){
        try{
            return this.prodMes.get(mes).getTot_Prod_P();
        }catch(NullPointerException e){return 0;}
    }
    
    /**
     * @param int mes
     * @return int
     * Metodo getMesCN
     */
    public int getMesCN(int mes){
        try{
            return this.prodMes.get(mes).getTot_Comp_N();
        }catch(NullPointerException e){return 0;}
    }
    
     /**
     * @param int mes
     * @return int
     * Metodo getMesCP
     */
    public int getMesCP(int mes){
        try{
            return this.prodMes.get(mes).getTot_Comp_P();
        }catch(NullPointerException e){return 0;}
    }
    

    public int hashCode(){return this.getCode().hashCode();}
    
     /**
     * @return ArrayList<String>
     * Metodo infoMes - devolve a informaçao mensal
     */
    public ArrayList<String> infoMes(){
        ArrayList<String> res = new ArrayList<>();
        for(int i=1;i<=12;i++){
            try{
                res.add(this.prodMes.get(i).toString());
            }catch(NullPointerException e){
                res.add("Este produto nao foi comprado neste mes");
            }
        }
        return res;
    }
}
