
/**
 * Write a description of class ComparatorCodeQuantMes here.
 * 
 * @author (your name) 
 * @version 27-05-2015
 */
import java.util.Comparator;
import java.io.Serializable;
public class ComparatorFichCompProd implements Comparator<FichCompProd >, Serializable {
    /**
     * @param  FichCompProd  c1
     * @param  FichCompProd  c2
     * @return ínt
     */
    public int compare(FichCompProd  c1, FichCompProd  c2) {
        if(c1.getQuantAnual()<c2.getQuantAnual()) return 1;
        if(c1.getQuantAnual()>c2.getQuantAnual()) return -1;
        return c1.getCode().compareTo(c2.getCode());
    }
}
