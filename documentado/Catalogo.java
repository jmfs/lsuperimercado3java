/**
 * Write a description of class Catalogo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.lang.IndexOutOfBoundsException;
import java.io.Serializable;

public class Catalogo implements Serializable
{
    private ArrayList<CatLetra> lista;


    /**
     * Constructor for objects of class Catalogo
     */
    public Catalogo(){this.lista = new ArrayList<>(26);}
    public Catalogo(ArrayList<CatLetra> l){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : l){res.add(c.clone());}
        this.lista=res;
    }

    /**
     * @param Catalogo c
     * Constructor for objects of class Catalogo
     */
    public Catalogo(Catalogo c){this.lista = c.getLista();}
    
    /**
     * @return ArrayList<CatLetra>
     * Método getLista
     */
    public ArrayList<CatLetra> getLista(){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : this.lista){res.add(c.clone());}
        return res;
    }
    
    /**
     * @param ArrayList<CatLetra> l
     * Método setLista
     */

    public void setLista(ArrayList<CatLetra> l){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : l){res.add(c.clone());}
        this.lista=res;
    }
    
    /**
     * @return Catalogo
     * Método clone- faz copia do objecto
     */
    public Catalogo clone(){return new Catalogo(this);}
    
    /**
     * @param String code
     * @return int
     * Método posicao
     */
    public int posicao(String code){return (code.charAt(0) -'A');}
    
    public void initCat(){
        for(int i=0;i<26;i++){
            CatLetra novo = new CatLetra((char)('A'+i));
            this.lista.add(i,novo);
        }
    }
    
    /**
     * @param  String code
     * Método addCod, adiciona codigo
     */
    public void addCod(String code){
        if(this.lista.size()==0){
            this.initCat();
            this.lista.get(posicao(code)).addCod(code);
        }
        else {this.lista.get(posicao(code)).addCod(code);}
    }
    
    /**
     * @return boolean
     * @param  String code
     * Método existeCode, adiciona codigo, true caso exista
     */
    public boolean existeCode(String code){
        try{
            return this.lista.get(posicao(code)).existeCode(code);
        }
        catch(IndexOutOfBoundsException e){
            return false;
        }
    }
}