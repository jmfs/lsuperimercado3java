
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version 01-06-2015
 */

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Scanner;
import java.util.Set;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileWriter;
import java.io.PrintWriter;
public class Main
{
    private static Hipermercado hiper = new Hipermercado();
    
    private static Scanner input = new Scanner(System.in);
  
    public static void main(String[] args){
        MENUS.printTitle();
        MENUS.printDE();
        if(loadInicio()){
            cicloPrinc();
        }
    }
    
    public static boolean loadInicio(){
        int op;
        boolean res=false;
        MENUS.printInicial();
        System.out.println("Diga a opção que deseja testar");
        System.out.print(">>");
        op=lerInt();
        switch(op){
            case 1 : res=loadTxt();break;
            case 2 : res=loadObj();break;
        }
        return res;
    }

    public static void cicloPrinc(){
        int op;
        do{
            MENUS.printPrinc();
            System.out.println("Diga a opção que deseja testar");
            System.out.print(">>");
            op=lerInt();
            switch(op){
                    case 1 : 
                    {
                        try{
                            reloadTxt();break;
                        }catch(NoReloadTxtException e){System.out.println(e.getMessage());}
                    }
                    case 2 : 
                    {
                        try{
                            reloadObj();break;
                        }catch(NoReloadObjException e){System.out.println(e.getMessage());}
                    }
                    case 3 : save();break;
                    case 4 : statusLast();break;
                    case 5 : queries();break;
            }
        }while(op>=1 && op<=5);   
    }

    public static void queries(){
        int op;
        do{
            MENUS.menuQueries();
            System.out.println("Diga a opção que deseja testar");
            System.out.print(">>");
            op=lerInt();
            switch(op){
                case 1 : querie1();break;
                case 2 : querie2();break;
                case 3 : querie3();break;
                case 4 : querie4();break;
                case 5 : querie5();break;
                case 6 : querie6();break;
                case 7 : querie7();break;
                case 8 : querie8();break;
                case 9 : querie9();break;
                case 10 : querie10();break;
            }
        }while(op>=1 && op<=10);
    }
    
    public static boolean loadTxt(){
        return ( lerCodigosClientes() && lerCodigosProdutos() && lerCompras());
    }

    public static boolean loadObj(){
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        FileNameExtensionFilter txtfilter = new FileNameExtensionFilter("obj files (*.obj)", "obj");
        chooser.setFileFilter(txtfilter);
        int option = chooser.showOpenDialog(chooser);
        if (option == JFileChooser.APPROVE_OPTION) {
            File sf = chooser.getSelectedFile();
            String filepath  = sf.getAbsolutePath();
            System.out.println("A Caregar o ficheiro " + sf.getName() + "\n");
            Crono.start();
            hiper=hiper.load(filepath);
            Crono.stop();
            System.out.println("Demorou " + Crono.print() + " seg a carregar " +  sf.getName() +"\n");
        }
        return true;
    }
    
    public static void statusLast(){
        int op;
        do{
            MENUS.menuEsta();
            System.out.println("Diga a opção que deseja testar");
            System.out.print(">>");
            op=lerInt();
            switch(op){
                case 1 : {System.out.print(hiper.getLastFile().toString());break;}
                case 2 : {statHiper();break;}
            }
        }while(op>=1 && op<=2);
    }
   
    
    public static void statHiper(){
        int op;
        System.out.println(hiper.statHiper());
        ArrayList<String> lista = hiper.compinv();
        System.out.println("Existem "+ lista.size() +" linhas de compras inválidas;");
        printLista(hiper.compinv());
        if(lista.size()>0){
            System.out.println("Deseja guarda as compras invalida em ficheiro(.txt). 1-Sim; Outro-Nao;");
            System.out.print(">>");
        }
        op=lerInt();
        if(op==1){compInvSave(lista);}
    }
    
    public static void compInvSave(ArrayList<String> lista ){
        System.out.println("Diga o nome que deseja dar ao ficheiro");
        System.out.print(">>");
        String filepath = lerTexto();
        try{
            File f = new File(filepath);
            if(f.exists() || f.isDirectory()){
                System.out.println("O ficheiro " + filepath +" já existe;"); 
            }
            else{
                PrintWriter gravarArq = new PrintWriter(new FileWriter(f));
                for (String l : lista ) { gravarArq.printf(l);}
            }
        }catch(IOException e){
            System.out.println("Nao foi possivel guardar a lista no ficheiro " + filepath +";"); 
        }
    }
    
    public static void reloadTxt() throws NoReloadTxtException{
        hiper = new Hipermercado();
        if(!loadTxt()){throw new NoReloadTxtException("Não foi possivel fazer reload;");}
    }

    public static void reloadObj() throws NoReloadObjException{
        if(!loadObj()){throw new NoReloadObjException("Não foi possivel fazer reload;");}
    }
    
    public static void querie1(){
        Crono.start();
        Set<String> listaSet =hiper.nuncaCompAnual();
        ArrayList<String> lista = new ArrayList<>(listaSet);
        System.out.println("Nao foram comprados " + lista.size() + " produtos.");
        Crono.stop();
        System.out.println("Tempo de consulta das estruturas para querie 1: " + Crono.print() + " seg.\n");
        print_formatado(lista);
        System.out.println("Tempo de execução da querie 1: " + Crono.print() + " seg.\n");
    }
    
    public static void querie2(){
        Crono.start();
        Set<String> listaSet =hiper.clientNuncaComp();
        ArrayList<String> lista = new ArrayList<>(listaSet);
        System.out.println("Tempo de consulta das estruturas para querie 2: " + Crono.print() + " seg.\n");
        System.out.println("Nao compraram " + lista.size() + " clientes.");
        Crono.stop();
        print_formatado(lista);
        System.out.println("Tempo de execução da querie 2: " + Crono.print() + " seg.\n");
    }
    
    public static void querie3(){
        System.out.printf("ARQUITETURA DE COMANDO: <MES>;\n>>");
        int mes = lerInt();
        Crono.start();
        try{
            System.out.print(hiper.infoMesComp(mes));
        }catch(NaoExisteMesException m){
            System.out.println(m.getMessage());
        }
        Crono.stop();
        System.out.println("Tempo de execução da querie 3: " + Crono.print() + " seg.\n");
    }
    
    public static void querie4(){
        int mes;
        System.out.printf("ARQUITETURA DE COMANDO: <CLIENTE>;\n>>");
        String code = lerPalavra();
        try{
            Crono.start();
            ArrayList<String> meses= hiper.infoMesCompClient(code);
            Crono.stop();
            System.out.println("Tempo de consulta das estruturas para querie 4: " + Crono.print() + " seg.\n");
            System.out.println("Informação Anual");
            System.out.print(meses.get(0));
            do{
                System.out.println("Diga o mes [1..12] ou outro para sair:");
                System.out.print(">>");
                mes=lerInt();
                try{
                    System.out.print(meses.get(mes));
                }catch(ArrayIndexOutOfBoundsException e){}
                catch(IndexOutOfBoundsException e){}
            }while(mes>=1 && mes<=12);
            System.out.println("Tempo de execução da querie 4: " + Crono.print() + " seg.\n");
        }catch(NaoExisteClienteException c){
            System.out.println(c.getMessage());
        }
    }
    
    public static void querie5(){
        int mes;
        System.out.printf("ARQUITETURA DE COMANDO: <PRODUTO>;\n>>");
        String code = lerPalavra();
        try{
            Crono.start();
            ArrayList<String> meses= hiper.infoMesCompProd(code);
            Crono.stop();
            System.out.println("Tempo de consulta das estruturas para querie 5: " + Crono.print() + " seg.\n");
            do{
                System.out.println("Diga o mes [1..12] ou outro para sair:");
                System.out.print(">>");
                mes=lerInt();
                try{
                    System.out.print(meses.get(mes-1));
                }catch(IndexOutOfBoundsException e){}
            }while(mes>=1 && mes<=12);
            System.out.println("Tempo de execução da querie 5: " + Crono.print() + " seg.\n");
        }catch(NaoExisteProdutoException p){
            System.out.println(p.getMessage());
        }catch(ProdutoSemFichaException pf){
            System.out.println(pf.getMessage());     
        }
    }
    
    //falta exception aqui
    public static void querie6(){
        int mes;
        System.out.printf("ARQUITETURA DE COMANDO: <PRODUTO>;\n>>");
        String code = lerPalavra();
        Crono.start();
        ArrayList<String> meses= hiper.infoMesesContbCode(code);
        Crono.stop();
        System.out.println("Tempo de consulta das estruturas para querie 6: " + Crono.print() + " seg.\n");
        do{
            System.out.println("Diga o mes [1..12] ou outro para sair:");
            System.out.print(">>");
            mes=lerInt();
            try{
                System.out.print(meses.get(mes-1));
            }catch(IndexOutOfBoundsException e){}
        }while(mes>=1 && mes<=12);
        System.out.println("Tempo de execução da querie 6: " + Crono.print() + " seg.\n");
    }
    
    public static void querie7(){
        System.out.printf("ARQUITETURA DE COMANDO: <CLIENTE>;\n>>");
        String client = lerPalavra();
        try{
            Crono.start();
            ArrayList<String> listaTop = hiper.topProdutosDoClient(client);
            Crono.stop();
            System.out.println("Tempo de consulta das estruturas para querie 7: " + Crono.print() + " seg.\n");
            printformatTOP(listaTop);
            System.out.println("Tempo de execução da querie 7: " + Crono.print() + " seg.\n");
        }catch(NaoExisteClienteException c){
            System.out.println(c.getMessage());
        }catch(ClienteSemFichaException cf){
            System.out.println(cf.getMessage());
        }
    }
    
    public static void querie8(){
        Crono.start();
        ArrayList<String> listaTop = hiper.topProdutos();
        System.out.println("O top tem "+ listaTop.size() +".\n");
        Crono.stop();
        System.out.println("Tempo de consulta das estruturas para querie 8: " + Crono.print() + " seg.\n");
        printformatTOP(listaTop);
        System.out.println("Tempo de execução da querie 8: " + Crono.print() + " seg.\n");
    }
    
    public static void querie9(){
        Crono.start();
        ArrayList<String> listaTop = hiper.topClientes();
        System.out.println("O top tem "+ listaTop.size() +" clientes.\n");
        Crono.stop();
        System.out.println("Tempo de consulta das estruturas para querie 9: " + Crono.print() + " seg.\n");
        printformatTOP(listaTop);
        System.out.println("Tempo de execução da querie 9: " + Crono.print() + " seg.\n");
    }
    
    public static void querie10(){
        System.out.printf("ARQUITETURA DE COMANDO: <PRODUTO>;\n>>");
        String code = lerPalavra();
        try{
            Crono.start();
            ArrayList<String> listaTop = hiper.topClientesDoProduto(code);
            System.out.println("O top tem "+ listaTop.size() +" produtos.\n");
            Crono.stop();
            System.out.println("Tempo de consulta das estruturas para querie 10: " + Crono.print() + " seg.\n");
            printformatTOP(listaTop);
            System.out.println("Tempo de execução da querie 10: " + Crono.print() + " seg.\n");
        }catch(NaoExisteProdutoException p){
            System.out.println(p.getMessage());
        }
    }
   
    public static void save(){
        StringBuilder s = new StringBuilder();
        StringBuilder aux = new StringBuilder();
        String path;
        File theDir = new File(".."+ File.separator +"Backups");

        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (SecurityException se) {

            }
        }
        System.out.print("Diga o nome do Ficheiro para Backup:\n>>");
        path = lerTexto();
        s.append(".."+File.separator + "Backups" + File.separator + path + ".obj");
        File f = new File(s.toString());
        if(f.exists() || f.isDirectory()) {
            System.out.println("O ficheiro " + s.toString() + " ja existe, operação abortada.");
            return;
        }
        Crono.start();
        System.out.println(hiper.save(s.toString()));
        Crono.stop();
        System.out.println("Demorou " + Crono.print() + " seg a guadar " +  s.toString() +"\n");
    }
    
    
    public static void print_formatado(ArrayList<String> lista){
        int total=lista.size();
        int topage,linhas=0,j,pos,paginas,avanco;
        int nlinhas,ncolunas;
        if(total>0){
            System.out.println("Nº linhas:");
            System.out.print(">>");
            nlinhas=lerInt();
            System.out.println("Nº colunas:");
            System.out.print(">>");
            ncolunas=lerInt();
            
            try{
                avanco=(nlinhas*ncolunas);
                paginas = (total / avanco);
                if((total % (nlinhas*ncolunas))==0) paginas--;
                for (topage=0,pos=topage*avanco; topage!=-1; ){
                    if(pos<total){
                        if(linhas<9){
                            System.out.printf("%d    ",linhas+1);
                        }else if(linhas<99){
                            System.out.printf("%d   ",linhas+1);
                        }else{
                            System.out.printf("%d  ",linhas+1);
                        }
                        for(j=0;j<ncolunas && pos<total;j++){
                            System.out.printf("%s\t\t",lista.get(pos));
                            pos++;
                        }
                        System.out.printf("\n");
                        linhas++;
                    }else{
                        linhas=nlinhas;
                    }
                    if(pos > 0 && linhas % nlinhas == 0 ){
                        if(paginas>0){
                            System.out.printf("Encontrasse na pagina %d.\nPara que pagina deseja continuar a imprimir? (<1 ou >%d SAIR)\n",topage+1,paginas+1);
                            topage=input.nextInt()-1;
                            pos=topage*avanco;
                            linhas=topage*nlinhas;
                        }else topage=1;
                    }
                    if(topage<0 || topage>paginas){
                        topage =-1;
                    }
                }
            }catch(ArithmeticException a){
                System.out.println("Numero de linhas e de colunas errado;");
            }
        }
    }
    
    public static void printformatTOP(ArrayList<String> listaTop){
        int topage,linhas=0,paginas;
        int total=listaTop.size();
        int nlinhas;
        if(total>0){
            System.out.println("Nº de linhas por pagina:");
            System.out.print(">>");
            nlinhas=lerInt();
            
            try{
                paginas = (total / nlinhas);
                if((total % nlinhas)==0) paginas--;
                for (topage=0,linhas=topage*nlinhas; topage!=-1 ; ){
                    if(linhas<total){
                        System.out.printf("Posição "+(linhas+1) + " :" + listaTop.get(linhas));
                        linhas++;
                    }else{
                        linhas=nlinhas;
                    }
                    if(linhas > 0 && linhas % nlinhas == 0 ){
                        if(paginas>0){
                            System.out.printf("\nEncontrasse na pagina %d.\nPara que pagina deseja continuar a imprimir? (<1 ou >%d SAIR)\n>>",topage+1,paginas+1);
                            topage=lerInt()-1;
                            System.out.printf("\n");
                            linhas=topage*nlinhas;
                        }else topage=1;
                    }
                    if(topage<0 || topage>paginas){
                        topage =-1;
                    }
                }
            }catch(ArithmeticException a){
                System.out.println("Numero de linhas erado");
            }
        }
    }
    
    public static void printLista(ArrayList<String> listaTop){
        int topage,linhas=0,paginas;
        int total=listaTop.size();
        int nlinhas;
        if(total>0){
            System.out.println("Nº de linhas por pagina:");
            System.out.print(">>");
            nlinhas=lerInt();
            
            try{
                paginas = (total / nlinhas);
                if((total % nlinhas)==0) paginas--;
                for (topage=0,linhas=topage*nlinhas; topage!=-1 ; ){
                    if(linhas<total){
                        System.out.print(listaTop.get(linhas));
                        linhas++;
                    }else{
                        linhas=nlinhas;
                    }
                    if(linhas > 0 && linhas % nlinhas == 0 ){
                        if(paginas>0){
                            System.out.printf("\nEncontrasse na pagina %d.\nPara que pagina deseja continuar a imprimir? (<1 ou >%d SAIR)\n>>",topage+1,paginas+1);
                            topage=lerInt()-1;
                            System.out.printf("\n");
                            linhas=topage*nlinhas;
                        }else topage=1;
                    }
                    if(topage<0 || topage>paginas){
                        topage =-1;
                    }
                }
            }catch(ArithmeticException a){
                System.out.println("Numero de linhas erado");
            }
        }
    }
    
    public static int lerInt(){
        while (!input.hasNextInt()) {
            System.out.println("Escreva um inteiro.");
            System.out.print(">>");
            input.nextLine();
        }
        int num = input.nextInt();
        return num;
    }

    public static String lerPalavra() {
       // int i=0;
        String s = input.nextLine();
        String[] array = s.split(" ");
        if(array[0].equals("")){
            s = input.nextLine();
            array = s.split(" ");
        }
        while(array.length>1 || array[0].equals("")){
            System.out.println("Repita(ERRO):");
            System.out.print(">>");
            s = input.nextLine();
            array = s.split(" ");
        }
        return array[0];
    }
    
    public static String lerTexto(){
        String nome = new String();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));   
        try{   
            nome = br.readLine();     
        }   
        catch (IOException io){   
            System.err.println(io.toString());   
        } 
        return nome;
    }
    
    /**
     * [lerCodigosClientes description]
     * @return [description]
     */
    public static boolean lerCodigosClientes(){
        Scanner fichScan;
        Crono.start();

        System.out.println("#============================#");
        System.out.println("#  Ler Ficheiro de Clientes  #");
        System.out.println("#============================#");
        String filepath =  (".."+File.separator+"files" + File.separator + "FichClientes.txt" );
       
        try {
            fichScan = new Scanner(new FileReader(filepath));
            fichScan.useDelimiter("\r\n");
            while(fichScan.hasNext()){
                String c = fichScan.next();
                hiper.addCodClient(c);
                hiper.initCompras(c);
            }
            Crono.stop();
            System.out.println(filepath+";");
            System.out.println("Demorou " + Crono.print() + " segs;" ); 
        }
        catch(IOException e){
            System.out.println("Não foi possivel ler o ficheiro");
            return false;
        }
        return true;  
    }
    
    /**
     * [lerCodigosProdutos description]
     * @return [description]
     */
    public static boolean lerCodigosProdutos(){
        Scanner fichScan;
      
        Crono.start();
        System.out.println("#============================#");
        System.out.println("#  Ler Ficheiro de Produtos  #");
        System.out.println("#============================#");
        String filepath = (".."+File.separator+"files" + File.separator + "FichProdutos.txt");
        
        try {
            fichScan = new Scanner(new FileReader(filepath));
            fichScan.useDelimiter("\r\n");
            while(fichScan.hasNext()){
                String c = fichScan.next();
                hiper.addCodProd(c);
                hiper.initContb(c);
            }
            Crono.stop();
            System.out.println(filepath+";");
            System.out.println("Demorou " + Crono.print() + " segs;" );
        }
        catch(IOException e){
            System.out.println("Não foi possivel ler o ficheiro");
            return false;
        } 
        return true;
    }
    
    
    public static boolean lerCompras(){
        String line;
        String codProd;double price;int quant;char modo;String codClient;int mes;
        String parts[];
        int i=1;
        int nLinhas=0;
        int nLinhasVal=0;
        int priceZero=0;
        int nProdQuant=0;
        double fat=0;
        StatFile lastFile = new StatFile();
        
        System.out.println("#============================#");
        System.out.println("#  Ler Ficheiro de Compras   #");
        System.out.println("#============================#");

        System.out.println("Diga o nome do ficheiro:");
        System.out.print(">>");
        String filepath  = lerTexto();
        Crono.start();
        Scanner fichScan = null;
        try {
            fichScan = new Scanner(new FileReader(filepath));
            fichScan.useDelimiter("\r\n");
            while(fichScan.hasNext()){
                nLinhas++; //incrementa o numero de linhas que esta a ler
                line=fichScan.next();
                parts = line.split(" ");
                codProd=parts[0];
                price=Double.parseDouble(parts[1]);
                quant=Integer.parseInt(parts[2]);
                modo=parts[3].charAt(0);
                codClient=parts[4];
                mes=Integer.parseInt(parts[5]);
                if(quant>=0 && mes>=1 && mes<=12 && price>=0 && (modo=='N' || modo=='P') && hiper.existeProd(codProd) && hiper.existeClient(codClient)){
                    if(price==0)priceZero++;
                    nProdQuant+=quant;
                    fat+=(quant*price);
                    hiper.carregaContb(codProd,price,quant,modo,mes);
                    hiper.updateCompras(codProd,price,quant,codClient,mes);  
                    nLinhasVal++;
                }
                else{
                    hiper.addCompInv(new CompINVAL(codProd,price,quant,modo,codClient,mes)); 
                }
            }
        }
        catch(IOException e){
            System.out.println("Nao foi possivel carregar o ficheiro " + filepath +";"); 
            return false;
        }
        lastFile.setPath(filepath);
        lastFile.setNLinhas(nLinhas);
        lastFile.setNLinhasVal(nLinhasVal);
        lastFile.setPriceZero(priceZero);
        lastFile.setFat(fat);
            
        lastFile.setNProd(nProdQuant);
        lastFile.setDistProd(hiper.getDistProd());
        lastFile.setNProdNoComp(hiper.getNProdNoComp());
           
        lastFile.setNClient(hiper.nClient());
        lastFile.setNClientComp(hiper.nClientComp());
        lastFile.setNClientNoComp(hiper.nClientNoComp());
            
        hiper.setLastFile(lastFile);
        lastFile.toString();
        Crono.stop();
        System.out.println("Demorou demorou " + Crono.print() + " segs a ler o ficheiro " + filepath +";");
        return true;
    }
}
