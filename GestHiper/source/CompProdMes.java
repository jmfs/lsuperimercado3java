
/**
 * Write a description of class CompProdMes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeSet;
import java.io.Serializable;
public class CompProdMes implements Serializable
{
    // instance variables - replace the example below with your own
    private double gasto;
    private int quant;
    private int comp;
    private int nClient;
   
    /**
     * Constructor for objects of class CompProdMes
     */
    public CompProdMes(){
        this.gasto=0;
        this.quant=0;
        this.comp=0;
        this.nClient=0;
    }
    
    public CompProdMes(double gasto, int quant, int nClient){
        this.gasto=gasto;
        this.quant=quant;
        this.comp++;
        this.nClient=nClient;
    }
    
    public CompProdMes(CompProdMes c){
        this.gasto=c.getGasto();
        this.quant=c.getQuant();
        this.comp=c.getComp();
        this.nClient=c.getNClient();
    }
    
    public double getGasto(){return this.gasto;}
    public int getQuant(){return this.quant;}
    public int getComp(){return this.comp;}
    public int getNClient(){return this.nClient;}
    
    public void setGasto(double g){this.gasto=g;}
    public void setQuant(int q){ this.quant=q;}
    public void setComp(int c){ this.comp=c;}
    public void setNClient(int n){ this.nClient=n;}
    
    public CompProdMes clone(){return new CompProdMes(this);}
    
    public void updateMes(double g, int q){
        this.gasto+=(g*q);
        this.quant+=q;
        this.comp++;
        this.nClient+=1;
    }
    
    public void updateMesJaComp(double g, int q){
        this.gasto+=(g*q);
        this.quant+=q;
        this.comp++;
    }
    
    public String info(){
        StringBuilder s = new StringBuilder();
        s.append("\tGasto: " + String.format("%.2f",this.gasto)+";\n");
        s.append("\tQuantidade vendida: "+ this.quant +";\n");
        s.append("\tNº de Compras: "+ this.comp+";\n");
        s.append("\tNº de Clientes distintos que compraram: "+this.nClient+";\n");
        return s.toString();
    }
}
