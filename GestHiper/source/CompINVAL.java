
/**
 * Write a description of class CompINVAL here.
 * 
 * @author (your name) 
 * @version 5-06-2015
 */

import java.io.Serializable;
public class CompINVAL implements Serializable
{
    private String codProd;
    private double price;
    private int quant;
    private char modo;
    private String codClient;
    private int mes;
   
    /**
     * Construtor vazio para objectos da classe Compra 
     */
    public CompINVAL(){
        this.codProd="";
        this.price=0;
        this.quant=0;
        this.modo='-';
        this.codClient="";
        this.mes=-1;
    }
    
    /**
     * Construtor parameterizado para objectos da classe Compra
     */
    public CompINVAL(String cP, double price, int quant, char modo, String cC, int m){
        this.codProd=cP;
        this.price=price;
        this.quant=quant;
        this.modo=modo;
        this.codClient=cC;
        this.mes=m;
    }
    
    /**
     * Construtor de copia para objectos da classe Compra
     */
    public CompINVAL(CompINVAL c){
        this.codProd=c.getCodProd();
        this.price=c.getPrice();
        this.quant=c.getQuant();
        this.modo=c.getModo();
        this.codClient=c.getCodClient();
        this.mes=c.getMes();
    }
    
    public String getCodProd(){return this.codProd;}
    public double getPrice(){return this.price;}
    public int getQuant(){return this.quant;}
    public char getModo(){return this.modo;}
    public String getCodClient(){return this.codClient;}
    public int getMes(){return this.mes;}
    
    public void setCodProd(String c){this.codClient=c;}
    public void setPrice(double price){this.price=price;}
    public void setQuant(int quant){this.quant=quant;}
    public void setModo(char m){this.modo=modo;}
    public void setCodClient(String c){this.codClient=c;}
    public void setMes(int m){this.mes=m;}
    
    /**
     * Metodo clone
     */
    public CompINVAL clone(){return new CompINVAL(this);}
    
    /**
     * Metodo equals
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass()) return false;
        CompINVAL c =(CompINVAL)obj;
        return (this.getCodProd().equals(c.getCodProd()) && this.getPrice()==c.getPrice()
                && this.getQuant()==c.getQuant() && this.getModo()==c.getModo()
                && this.getCodClient().equals(c.getCodClient()) && this.getMes()==c.getMes());
    }
    
    /**
     * Metodo toString
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("===================================================\n");
        s.append("Codigo de Produto: " + this.getCodProd() +";\n");
        s.append("Preço: " + this.getPrice() +";\n");
        s.append("Quantidade: " + this.getQuant() +";\n");
        s.append("Modo: " + this.getModo() +";\n");
        s.append("Codigo de Cliente: " + this.getCodClient() +";\n");
        s.append("Mes: " + this.getMes() +";\n");
        return s.toString();
    }
}
