
/**
 * Write a description of class Compras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.Serializable;
import java.util.List;

public class Compras implements Serializable
{
    // instance variables
    private TreeMap<String,FichCompClient> compClient;// os clientes tem de ser colocados todos ao inicio com valeu = a null
    private TreeMap<String,FichCompProd> compProd;// os produtos nao precisao de ser inicializados ao inicio
    private ArrayList<TopMes> ano;//para responder a querie 3//so vai ser aumentado o contador do clietn se nao existir no TreeMap do client

    public Compras(){
        this.compClient=new TreeMap<>();
        this.compProd=new TreeMap<>();
        this.ano=new ArrayList<>(13);
    }
  
    public Compras(Compras c){
        this.compClient= c.getCompClient();
        this.compProd=c.getCompProd();
        this.ano=c.getAno();
    }
    
    public TreeMap<String,FichCompClient> getCompClient(){
        TreeMap<String,FichCompClient> res = new TreeMap<>();
        Iterator<String> it = this.compClient.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            try{
                res.put(n, this.compClient.get(n).clone());
            }catch(NullPointerException e){
                res.put(n,null);
            }
        }
        return res;
    }
    
    public TreeMap<String,FichCompProd> getCompProd(){
        TreeMap<String,FichCompProd> res = new TreeMap<>();
        Iterator<String> it = this.compProd.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            try{
                res.put(n, this.compProd.get(n).clone());
            }catch(NullPointerException e){
                res.put(n,null);
            }
        }
        return res;
    }
    
    public ArrayList<TopMes> getAno(){
        ArrayList<TopMes> res = new ArrayList<>();
        for(int i=1;i<=12;i++){res.add(i,this.ano.get(i).clone());}
        return res;
    }
    
    public Compras clone(){return new Compras(this);}
    
    public void initCompClient(String client){this.compClient.put(client, null);}
   
    private void initAno(){for(int i=0;i<=12;i++){this.ano.add(new TopMes());}}
    
    public void updateCompras(String prod, double price,int quant,String client, int mes){
        if(this.ano.size()==0) this.initAno();
        //parte do cliente
        //se está a null
        if(this.compClient.get(client)==null){
            //caso o cliente ainda nao tenha comprado
            FichCompClient fc = new FichCompClient(client);
            fc.initClientInfoM();
            fc.updateClient(prod, mes, price, quant);
            this.compClient.put(client, fc);
            this.ano.get(mes).updateMesNoComprou(price, quant);
            this.ano.get(0).updateMesNoComprou(price, quant);
        }
        else{
            //caso o cliente já tenha ficha ou seja já comprou
            this.compClient.get(client).updateClient(prod, mes, price, quant);
            if(this.compClient.get(client).comprouMes(mes)){
                this.ano.get(mes).updateMesJáComprou(price, quant);
                this.ano.get(0).updateMesJáComprou(price, quant);
            }
            else {
                this.ano.get(mes).updateMesNoComprou(price, quant);
                this.ano.get(0).updateMesNoComprou(price, quant);
            }
        }
        //parte do produto
        if(this.compProd.containsKey(prod)){
            this.compProd.get(prod).updateProd(client, price, quant, mes);
        }
        else{
            FichCompProd fp = new FichCompProd(prod);
            fp.initMesCtr();
            fp.updateProd(client, price, quant, mes);
            this.compProd.put(prod, fp);
        }
    }
    
    public String infoMes(int mes) throws NaoExisteMesException {
        TopMes t;
        StringBuilder s = new StringBuilder();
        if(mes<=0 || mes>12) {throw new NaoExisteMesException("O mes " + mes + " nao existe");}
        if((t = this.ano.get(mes))==null){ throw new NaoExisteMesException("O mes " + mes + " nao tem informação guarda");};
        s.append("Informaçao do Mes " + mes+":\n");
        s.append("\tFaturação: " + String.format("%.2f", t.getGasto())+"\n");
        s.append("\tQuantidade Produtos: "+ t.getQuant()+"\n");
        s.append("\tNº de Compras: "+ t.getComp()+"\n");
        s.append("\tNº de Clientes distintos que compraram: "+t.getNumDist()+"\n");
        return s.toString();
    }

    public ArrayList<String> infoMesesCliente(String client) throws NaoExisteClienteException{
        ArrayList<String> res = new ArrayList<>();
        if(!this.compClient.containsKey(client)) {throw new NaoExisteClienteException("O Cliente "+ client + " nao existe;");}
        for(int mes=0;mes<=12;mes++){
            res.add(this.compClient.get(client).infoMes(mes));
        }
        return res;
    }
    
    public ArrayList<String> infoMesesProd(String prod) throws ProdutoSemFichaException{
        ArrayList<String> res = new ArrayList<>();
        if(!this.compProd.containsKey(prod)) {throw new ProdutoSemFichaException("O produto " + prod +" nunca foi comprado;");}
        for(int mes=1;mes<=12;mes++){
            res.add(this.compProd.get(prod).infoMes(mes));
        }
        return res;
    }
    
    public ArrayList<String> getClientTopProd(String prod) throws NaoExisteProdutoException{
        if(!this.compProd.containsKey(prod)) throw new NaoExisteProdutoException("O Produto "+ prod + " nunca foi comprado ;");
        return this.compProd.get(prod).topProdutoCliente();
    }
    
    public ArrayList<String> getProdutoTopClientes(String client) throws NaoExisteClienteException, ClienteSemFichaException {
        if(!this.compClient.containsKey(client)) throw new NaoExisteClienteException("O Cliente "+ client + "não existe;");
        if(this.compClient.get(client)==null) throw new ClienteSemFichaException("O Cliente " + client +"nao comprou nenhum Produto");
        return this.compClient.get(client).topClientProdutos();
    }
    
    public ArrayList<String> getClientTop(){
        TreeSet<FichCompClient> topR = new TreeSet<>(new ComparatorFichCompClient());
        Iterator<FichCompClient> itK = this.compClient.values().iterator();
        while(itK.hasNext()){
            topR.add(itK.next().clone());
        }
        ArrayList<String> res = new ArrayList<>();
        Iterator<FichCompClient> it = topR.iterator();
        while(it.hasNext()){
            res.add(it.next().infoFichCompClient());
        }
        return res;
    }
    
    public ArrayList<String> getProdTop(){
        TreeSet<FichCompProd> topR = new TreeSet<>(new ComparatorFichCompProd());
        Iterator<FichCompProd> itK = this.compProd.values().iterator();
        while(itK.hasNext()){
            topR.add(itK.next().clone());
        }
        ArrayList<String> res = new ArrayList<>();
        Iterator<FichCompProd> it = topR.iterator();
        while(it.hasNext()){
            res.add(it.next().infoFichCompProd());
        }
        return res;
    }
    
    
    public Set<String> getNoFich(){
        TreeSet<String> res = new TreeSet<>();
        Iterator<String> it = this.compClient.keySet().iterator();
        while(it.hasNext()){
            String code = it.next();
            if(this.compClient.get(code) == null){
                res.add(code);
            }
        }
        return res;
    }
    
    public Set<String> nuncaCompraram(){
        return this.getNoFich();
    }
    
    public int sizeCompClient(){return this.compClient.size();}
    
    public int nClientComp(){
        int res=0;
        Iterator<String> it = this.compClient.keySet().iterator();
        while(it.hasNext()){
            String code = it.next();
            if(this.compClient.get(code) != null) res++;
        }
        return res;
    }
    
    public int nClientNoComp(){
        int res=0;
        Iterator<String> it = this.compClient.keySet().iterator();
        while(it.hasNext()){
            String code = it.next();
            if(this.compClient.get(code) == null) res++;
        }
        return res;
    }
    
    public int getMesComp(int mes){return this.ano.get(mes).getComp();}
    public int getMesCl(int mes){return this.ano.get(mes).getNumDist();}
    public double getMesFat(int mes){return this.ano.get(mes).getGasto();}
}
