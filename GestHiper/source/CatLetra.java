
/**
 * Write a description of class CatLetra here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.TreeSet;
import java.util.TreeMap;
import java.io.Serializable;

public class CatLetra implements Serializable
{
    private char letra;
    private TreeSet<String> listaC;
    
    /**
     * Constructor for objects of class CatLetra
     */
    public CatLetra(){
        this.letra='-';
        this.listaC=new TreeSet<>();
    }
    
    public CatLetra(char l){
        this.letra=l;
        this.listaC = new TreeSet<>();
    }
    
    
    public CatLetra(char l, TreeSet<String> lista){
        this.letra=l;
        this.listaC = new TreeSet<>(lista);
    }
    
    public CatLetra(CatLetra l){
        this.letra=l.getLetra();
        this.listaC=l.getListaC();
    }
    
    public char getLetra(){return this.letra;}
    public TreeSet<String> getListaC(){return new TreeSet<>(this.listaC);}

    public void set(char l){this.letra=l;}
    public void set(TreeSet<String> lista){this.listaC=new TreeSet<>(lista);}
    
    public CatLetra clone(){return new CatLetra(this);}
    
    public boolean addCod(String code){return this.listaC.add(code);}
    
    public boolean existeCode(String code){return this.listaC.contains(code);}
}


