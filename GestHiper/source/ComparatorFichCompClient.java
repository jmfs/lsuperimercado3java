
/**
 * Write a description of class ComparatorCodeQuantMes here.
 * 
 * @author (your name) 
 * @version 27-05-2015
 */
import java.util.Comparator;
import java.io.Serializable;
public class ComparatorFichCompClient implements Comparator<FichCompClient>, Serializable {
    
    public int compare(FichCompClient c1, FichCompClient c2) {
        if(c1.getNumProd()<c2.getNumProd()) return 1;
        if(c1.getNumProd()>c2.getNumProd()) return -1;
        return c1.getCode().compareTo(c2.getCode());
    }
}
