
/**
 * Write a description of class Hipermercado here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Set;
import java.util.List;
import java.util.Iterator;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hipermercado implements Serializable
{
    private Catalogo catClient;
    private Catalogo catProd;
    private Contabilidade contb;
    private Compras comp;
 
    private StatFile lastFile;
    private ArrayList<CompINVAL> inv;
    
    /**
     * Constructor for objects of class Hipermercado
     */
    public Hipermercado(){
        this.catClient = new Catalogo();
        this.catProd = new Catalogo();
        this.contb=new Contabilidade();
        this.comp=new Compras();
        this.lastFile=new StatFile();
        this.inv = new ArrayList<>();
    }
    
    public Catalogo getCatClient(){return this.catClient.clone();}
    public Catalogo getCatProd(){return this.catProd.clone();}
    public Contabilidade getContb(){return this.contb.clone();}
    public Compras getComp(){return this.comp.clone();}
    public StatFile getLastFile(){return this.lastFile.clone();}
    public ArrayList<CompINVAL> getInv(){
        ArrayList<CompINVAL> res = new ArrayList<>();
        Iterator<CompINVAL> it = this.inv.iterator();
        while(it.hasNext()){
            res.add(it.next().clone());
        }
        return res;
     }
    
    public void setLastFile(StatFile s){this.lastFile = s;}
    
    //--------------Catalogos Clientes -------------------
    
    public void addCodClient(String code){this.catClient.addCod(code);}
    
    public void addListaClient(TreeSet<String> lista){
        for(String c : lista){this.addCodClient(c);}
    }
    
    public boolean existeClient(String code){return this.catClient.existeCode(code);}
    
    //--------------Catalogos Produtos -------------------
    
    public void addCodProd(String code){this.catProd.addCod(code);}
    
    public void addListaProd(TreeSet<String> lista){
        for(String c : lista){this.addCodProd(c);}
    }

    public boolean existeProd(String code){return this.catProd.existeCode(code);}
    
    //--------------Contabilidade---------------
    
    public void initContb(String code){
        this.contb.initContbCode(code);
    }
    
    public void carregaContb(String codProd, double price, int quant, char modo, int mes){
        switch(modo){
            case 'P' : {
                this.contb.updateCodeContb(codProd,mes, price, 0, quant, 0, 1, 0);
                break;
            }
            
            case 'N' : {
                this.contb.updateCodeContb(codProd,mes,0 , price,0 , quant, 0, 1);
                break;
            }
        }
    }
    
    public Set<String> nuncaCompAnual(){
        return this.contb.nuncaComprados();
    }
    
    //Falta ver exceptionaqui
    public ArrayList<String> infoMesesContbCode(String prod){
        return this.contb.infoMesesContb(prod);
    }
  
    public int getDistProd(){return this.contb.nProdComp();}
    
    public int getNProdNoComp(){return this.contb.nProdNoComp();}
    
    //-------------Compras--------------------
    
    public void initCompras(String listaClientes){this.comp.initCompClient(listaClientes);}
    
    public void updateCompras(String codProd, double price, int quant, String codClient, int mes){
        this.comp.updateCompras(codProd, price, quant, codClient,mes);
    }
    
    
    public Set<String> clientNuncaComp(){
        return this.comp.nuncaCompraram();
    }

    public String infoMesComp(int mes) throws NaoExisteMesException{
        return this.comp.infoMes(mes);
    }
    
    public ArrayList<String> infoMesCompClient(String client) throws NaoExisteClienteException{
        return this.comp.infoMesesCliente(client);
    }
    

    public ArrayList<String> infoMesCompProd(String prod) throws NaoExisteProdutoException,ProdutoSemFichaException{
        if(!this.catProd.existeCode(prod)) {throw new NaoExisteProdutoException("O produto " + prod + "nao existe;");}
        return this.comp.infoMesesProd(prod);
    }
    
    
    public ArrayList<String> topProdutosDoClient(String client) throws NaoExisteClienteException,ClienteSemFichaException  {
        return this.comp.getProdutoTopClientes(client);
    }
    
    public ArrayList<String> topClientesDoProduto(String prod) throws NaoExisteProdutoException{
        return this.comp.getClientTopProd(prod);
    }
    
    public ArrayList<String> topProdutos(){
        return this.comp.getProdTop();
    }
    
    public ArrayList<String> topClientes(){
        return this.comp.getClientTop();
    }
    
    public int nClient(){return this.comp.sizeCompClient();}
    
    public int nClientComp(){return this.comp.nClientComp();}
  
    public int nClientNoComp(){return this.comp.nClientNoComp();}
    
    //----------Geral------------
    
    public String save(String path){
        StringBuilder s = new StringBuilder();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
            
            oos.writeObject(this);
            oos.close();
           
        } catch (FileNotFoundException ex) {
            s.append("Nao foi encontrado o ficheiro com o path " + path + "\n");
            return s.toString();
        } catch (IOException ex) {
            return ex.getMessage();
        }
        return s.append("Estado guardado em : " + path+";\n").toString();
    }
    
    public Hipermercado load(String path){
        Hipermercado geo = new Hipermercado();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
            geo=((Hipermercado)ois.readObject());
            ois.close();
            return geo;
            }
        catch(StreamCorruptedException ex){
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    public boolean addCompInv(CompINVAL c){return this.inv.add(c);}
    
    public String statHiper(){
        StringBuilder s = new StringBuilder();
        s.append("=========================================================\n");
        s.append("  Mes   |   Nº Compras  |  Nº Clientes  |    Faturação   \n");
        s.append("========|===============|===============|================\n");
        s.append(" Mes 1  |     " +this.comp.getMesComp(1)  +  "     |      " + this.comp.getMesCl(1)  + "     |   " +  String.format("%.2f",this.comp.getMesFat(1))  + "€   \n");
        s.append(" Mes 2  |     " +this.comp.getMesComp(2)  +  "     |      " + this.comp.getMesCl(2) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(2))  + "€   \n");
        s.append(" Mes 3  |     " +this.comp.getMesComp(3)  +  "     |      " + this.comp.getMesCl(3) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(3))  + "€   \n");
        s.append(" Mes 4  |     " +this.comp.getMesComp(4)  +  "     |      " + this.comp.getMesCl(4) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(4))  + "€   \n");
        s.append(" Mes 5  |     " +this.comp.getMesComp(5)  +  "     |      " + this.comp.getMesCl(5) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(5))  + "€   \n");
        s.append(" Mes 6  |     " +this.comp.getMesComp(6)  +  "     |      " + this.comp.getMesCl(6) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(6))  + "€   \n");
        s.append(" Mes 7  |     " +this.comp.getMesComp(7)  +  "     |      " + this.comp.getMesCl(7) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(7))  + "€   \n");
        s.append(" Mes 8  |     " +this.comp.getMesComp(8)  +  "     |      " + this.comp.getMesCl(8) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(8))  + "€   \n");
        s.append(" Mes 9  |     " +this.comp.getMesComp(9)  +  "     |      " + this.comp.getMesCl(9) +  "     |   " +  String.format("%.2f",this.comp.getMesFat(9))  + "€   \n");
        s.append(" Mes 10 |     " +this.comp.getMesComp(10) +  "     |      " + this.comp.getMesCl(10) + "     |   " +  String.format("%.2f",this.comp.getMesFat(10)) + "€   \n");
        s.append(" Mes 11 |     " +this.comp.getMesComp(11) +  "     |      " + this.comp.getMesCl(11) + "     |   " +  String.format("%.2f",this.comp.getMesFat(11)) + "€   \n");
        s.append(" Mes 12 |     " +this.comp.getMesComp(12) +  "     |      " + this.comp.getMesCl(12) + "     |   " +  String.format("%.2f",this.comp.getMesFat(12)) + "€   \n");
        s.append("=========================================================\n");
        s.append("Nº total de Compras: " + this.comp.getMesComp(0) +";\n");
        s.append("Nº total de Clientes: " + this.comp.getMesCl(0) +";\n");
        s.append("Total Faturado: " + String.format("%.2f",this.comp.getMesFat(0)) +"€;\n");
        return s.toString();
    }
    
    public ArrayList<String> compinv(){
        ArrayList<String> res = new ArrayList<>();
        for(CompINVAL c : this.inv){
            res.add(c.toString());
        }
        return res;
    }
}
