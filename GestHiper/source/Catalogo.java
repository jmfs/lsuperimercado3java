
/**
 * Write a description of class Catalogo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.lang.IndexOutOfBoundsException;
import java.io.Serializable;

public class Catalogo implements Serializable
{
    private ArrayList<CatLetra> lista;


    /**
     * Constructor for objects of class Catalogo
     */
    public Catalogo(){this.lista = new ArrayList<>(26);}
    public Catalogo(ArrayList<CatLetra> l){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : l){res.add(c.clone());}
        this.lista=res;
    }
    public Catalogo(Catalogo c){this.lista = c.getLista();}
    
    
    public ArrayList<CatLetra> getLista(){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : this.lista){res.add(c.clone());}
        return res;
    }
    
    public void setLista(ArrayList<CatLetra> l){
        ArrayList<CatLetra> res = new ArrayList<>(26);
        for(CatLetra c : l){res.add(c.clone());}
        this.lista=res;
    }
    
    public Catalogo clone(){return new Catalogo(this);}
    
    public int posicao(String code){return (code.charAt(0) -'A');}
    
    public void initCat(){
        for(int i=0;i<26;i++){
            CatLetra novo = new CatLetra((char)('A'+i));
            this.lista.add(i,novo);
        }
    }
    
    public void addCod(String code){
        if(this.lista.size()==0){
            this.initCat();
            this.lista.get(posicao(code)).addCod(code);
        }
        else {this.lista.get(posicao(code)).addCod(code);}
    }
    
    public boolean existeCode(String code){
        try{
            return this.lista.get(posicao(code)).existeCode(code);
        }
        catch(IndexOutOfBoundsException e){
            return false;
        }
    }
}
