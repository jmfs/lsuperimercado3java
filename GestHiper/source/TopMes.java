
/**
 * Write a description of class TopMes here.
 * 
 * @author (your name) 
 * @version 28-05-2015
 */
import java.util.TreeSet;
import java.util.Iterator;
import java.util.TreeMap;
import java.io.Serializable;
public class TopMes implements Serializable
{

    private double gasto;
    private int quant;
    private int comp;
    private int numDist;
    
    public TopMes(){
        this.gasto=0;
        this.quant=0;
        this.comp=0;
        this.numDist=0;
    }
    
    public TopMes (TopMes t){
        this.gasto=t.getGasto();
        this.quant=t.getQuant();
        this.comp=t.getComp();
        this.numDist=t.getNumDist();
    }
    
    public double getGasto(){return this.gasto;}
    public int getQuant(){return this.quant;}
    public int getComp(){return this.comp;}
    public int getNumDist(){return this.numDist;}
    
    public void setGasto(double gasto){this.gasto=gasto;}
    public void setQuant(int quant){this.quant=quant;}
    public void setComp(int comp){this.comp=comp;}
    public void setnumDist(int nP){this.numDist=nP;}

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Gasto: " +this.getGasto() + "\n");
        s.append("Quantidade: " + this.getQuant() +"\n");
        s.append("Nº de compras: " + this.getComp() + "\n");
        s.append("Numero de Produtos distintos: " + this.getNumDist()+"\n");
        return s.toString();
    }

    public TopMes clone(){return new TopMes(this);}
    
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        TopMes c = (TopMes)obj;
        return (this.gasto==c.getGasto() && this.quant==c.getQuant()
               && this.comp==c.getComp() && this.numDist==c.getNumDist());
    }
    
    public void updateMesNoComprou(double price, int quant){
        this.gasto+=(price*quant);
        this.quant+=quant;
        this.comp++;
        this.numDist++;
    }
    
    public void updateMesJáComprou(double price, int quant){
        this.gasto+=(price*quant);
        this.quant+=quant;
        this.comp++;
    }
    
    public String info(){
        StringBuilder s = new StringBuilder();
        s.append("\tGasto: " + String.format("%.2f",this.gasto)+"€;\n");
        s.append("\tQuantidade Produtos: "+ this.quant+";\n");
        s.append("\tNº de Compras: "+ this.comp+";\n");
        s.append("\tNº de Produtos distintos comprados: "+this.numDist+";\n");
        return s.toString();
    }
}
