
/**
 * Write a description of class Menus here.
 * 
 * @author (your name) 
 * @version 29-05-2015
 * @version 01-06-2015
 */
public class MENUS
{
    public static void printTitle(){
        System.out.print("\n\n\t\t \033[1m\033[34m____    ____    ____    ______  __  __  ______   ____    ____    ____\033[0m       \n");
        System.out.print("\t\t\033[1m\033[34m/\\  _`\\ /\\  _`\\ /\\  _`\\ /\\__  _\\/\\ \\/\\ \\/\\__  _\\ /\\  _`\\ /\\  _`\\ /\\  _`\\\033[0m     \n");
        System.out.print("\t\t\033[1m\033[34m\\ \\ \\L\\_\\ \\ \\L\\_\\ \\,\\L\\_\\/_/\\ \\/\\ \\ \\_\\ \\/_/\\ \\/ \\ \\ \\L\\ \\ \\ \\L\\_\\ \\ \\L\\ \\\033[0m   \n");
        System.out.print("\t\t \033[1m\033[34m\\ \\ \\L_L\\ \\  _\\L\\/_\\__ \\  \\ \\ \\ \\ \\  _  \\ \\ \\ \\  \\ \\ ,__/\\ \\  _\\L\\ \\ ,  /\033[0m   \n");
        System.out.print("\t\t  \033[1m\033[34m\\ \\ \\/, \\ \\ \\L\\ \\/\\ \\L\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\_\\ \\__\\ \\ \\/  \\ \\ \\L\\ \\ \\ \\\\ \\\033[0m  \n");
        System.out.print("\t\t   \033[1m\033[34m\\ \\____/\\ \\____/\\ `\\____\\ \\ \\_\\ \\ \\_\\ \\_\\/\\_____\\\\ \\_\\   \\ \\____/\\ \\_\\ \\_\\\033[0m\n");
        System.out.print("\t\t    \033[1m\033[34m\\/___/  \\/___/  \\/_____/  \\/_/  \\/_/\\/_/\\/_____/ \\/_/    \\/___/  \\/_/\\/_/\033[0m\n\n\n");
    }  

    public static void printDE(){
        System.out.print("\n#==============================================#\n");
        System.out.print("#LEI Uminho 2014/2015                          #\n");
        System.out.print("#LI3 Projecto Java                             #\n");
        System.out.print("#Grupo 30:                                     #\n");
        System.out.print("#\tJoão Manuel Ferreira Da Silva - A72023 #\n");
        System.out.print("#\tJoão Tiago Pereira Dias - A72095       #\n");
        System.out.print("#\tRui Jorge Araújo Freitas - A72399      #\n");
        System.out.println("#==============================================#\n");    
    }

    public static void printInicial(){
        System.out.println("\t#==========GESTHIPER==========#");
        System.out.println("\t#                             #");
        System.out.println("\t#    1 - load (.txt) file     #");
        System.out.println("\t#    2 - load (.obj) file     #");
        System.out.println("\t#    Outro - exit             #");
        System.out.println("\t#                             #");
        System.out.println("\t#=============================#");
    }
    
    public static void printPrinc(){
        System.out.println("\t#==========GESTHIPER==========#");
        System.out.println("\t#                             #");
        System.out.println("\t#   1 - reload (.txt) file    #");
        System.out.println("\t#   2 - reload (.obj) file    #");
        System.out.println("\t#   3 - Save (.obj) file      #");
        System.out.println("\t#   4 - Estatisticas          #");
        System.out.println("\t#   5 - Queries               #");
        System.out.println("\t#   Outro - exit              #");
        System.out.println("\t#                             #");
        System.out.println("\t#=============================#");
    }
    
    public static void menuEsta(){
        System.out.println("\t#==========GESTHIPER==========#");
        System.out.println("\t#                             #");
        System.out.println("\t#   1 - Last file             #");
        System.out.println("\t#   2 - Estrutura             #");
        System.out.println("\t#   Outro - exit              #");
        System.out.println("\t#                             #");
        System.out.println("\t#=============================#");
    
    }
    
    public static void menuQueries(){
        System.out.println("#===========================================================Queries===========================================================================#");
        System.out.println("#                                                                                                                                             #");
        System.out.println("#   1 - Lista ordenada de todos os codigos de produtos que nunca foram comprados e o seu total;                                               #");
        System.out.println("#   2 - Lista ordenada de todos os códigos de clientes que nunca compraram e o seu total;                                                     #");
        System.out.println("#   3 - Dado um mês válido, determinar o número total de compras e o total de clientes distintos que as realizaram;                           #");
        System.out.println("#   4 - Dado um código de cliente, determinar, para cada mês, a faturação detalhada;                                                          #");
        System.out.println("#   5 - Dado o código de um produto, determinar, mês a mês, quantas vezes foi comprado, por quantos clientes diferentes e o total facturado;  #");
        System.out.println("#   6 - Dado o código de um produto, determinar, mês a mês, quantas vezes foi comprado em modo N e em modo P e respectivas facturações;       #");
        System.out.println("#   7 - Dado o código de um cliente determinar a lista de códigos de produtos que mais comprou;                                               #");
        System.out.println("#   8 - Determinar o conjunto dos produtos mais vendidos em todo o ano;                                                                       #");
        System.out.println("#   9 - Determinar os clientes que compraram um maior número de diferentes produtos, indicando quantos;                                       #");
        System.out.println("#   10 - Dado o código de um produto, determinar o conjunto dos clientes que mais o compraram e qual o valor gasto;                           #");
        System.out.println("#   Outro - exit;                                                                                                                             #");    
        System.out.println("#                                                                                                                                             #");
        System.out.println("#=============================================================================================================================================#");
    }
    
}
